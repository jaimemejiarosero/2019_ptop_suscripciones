<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class suscripciones extends Model
{
    protected $table = 'suscripciones';
    public $timestamps = false;
}
