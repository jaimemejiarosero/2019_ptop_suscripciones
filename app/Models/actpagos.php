<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class actpagos extends Model
{
    protected $table = 'actpagos';
    public $timestamps = false;
}
