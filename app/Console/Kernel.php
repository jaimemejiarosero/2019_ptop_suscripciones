<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Models;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            //CREAMOS DATOS PARA LA AUTENTIFICACION EN PLACE TO PAY
            $seed = date('c');
            if (function_exists('random_bytes')) {
                $nonce = bin2hex(random_bytes(16));
            } elseif (function_exists('openssl_random_pseudo_bytes')) {
                $nonce = bin2hex(openssl_random_pseudo_bytes(16));
            } else {
                $nonce = mt_rand();
            }

            $nonceBase64 = base64_encode($nonce);
            $secretKey = env('PTOP_DELTA_SECRETKEY');
            $login = env('PTOP_DELTA_LOGIN');


            $tranKey = base64_encode(sha1($nonce . $seed . $secretKey, true));
            $totPorPagar = 20;
            $fam_secuencia = 20;

            $alumnos = Models\acmalumna::where('fam_secuencia', '=', $fam_secuencia)->get();
            $pagador = [
                "documentType" => 'CI',
                "document" => '0401356035',
                "name" => 'Giovanny',
                "surname" => 'Yánez',
                "email" => 'giovanny.yanez@ppm.com.ec',
                "mobile" => '099765432',
            ];
            $instrument = [
                "token" => [
                    "token" => '0b394833c428845d61f9f86f8166a0e06afcb062c60d50882943914b4b849e8e',
                ],
            ];

            $reference = 'COB_' . time();

            $request = [
                "auth" => [
                    "login" => $login,
                    "seed" => $seed,
                    "nonce" => $nonceBase64,
                    "tranKey" => $tranKey
                ],
                "instrument" => $instrument,
                "locale" => 'es_EC',
                "payer" => $pagador,
                "payment" => [
                    "reference" => $reference,
                    "description" => "Cobro por suscripción desde CRON",
                    "amount" => [
                        "currency" => "USD",
                        "total" => round($totPorPagar, 2),
                        "taxes" => null,
                    ]
                ]
            ];
            $data = json_encode($request);
            //dd($data);
            //SOLICITAMOS LA CREACION DE SESSION PARA EL PAGO PLACETOPAY

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://test.placetopay.ec/redirection/api/collect",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $data,
                CURLOPT_HTTPHEADER => array(
                    "Cache-Control: no-cache",
                    "Content-Type: application/json",
                    "Postman-Token: 96983e26-ba1b-4dfa-bbe9-54f877428d08"
                ),
                CURLOPT_USERAGENT => \request()->header('User-Agent'),
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            if ($err) {
                return $err;
            } else {
                $resArray = json_decode($response,true);
                if($resArray['status']['status'] == 'FAILED'){
                    return $resArray;
                }
                //creamos el registro que relaciona pagos con alumnos y familiares
                $pagAlu = new Models\actalupagos();
                $pagAlu->alu_codalum = $alumnos[0]['alu_codalum'];
                $pagAlu->requestId = $resArray['requestId'];
                if($resArray['status']['status'] == 'APPROVED'){
                    $pagAlu->status = 'A';
                }else {
                    if ($resArray['status']['status'] == 'REJECTED') {
                        $pagAlu->status = 'R';
                    }else{
                        $pagAlu->status = 'P';
                    }
                }
                $pagAlu->save();
                //creamos el registro del pago en la bd cliente
                $pago = new Models\actpagos();
                $pago->requestId = $resArray['requestId'];
                $pago->date = date("Y-m-d H:i:s");
                $pago->status = $resArray['status']['status'];
                $pago->message = $resArray['status']['message'];;
                $pago->reference = $reference;
                $pago->signature = '';
                $pago->url = '';
                $pago->monto = round($totPorPagar,2);
                $pago->tipo_transaccion = 'SUS';
                //if($resArray['status']['status'] != 'PENDIEN'){
                //    $pago->detalle = $resArray['payment']['status']['message'];
                //}
                $pago->save();

                return $resArray;
            }
        })->everyMinute();;
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
