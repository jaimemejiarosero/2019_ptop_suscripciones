<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models;
use App;

class Controller extends BaseController
{

    public function pruebas(){
        return view('pruebas');
    }

    public function returnplacetopay(Request $request){
        return $request;
    }


    public function conPruebas(Request $request)
    {
        $secretKey = env('PTOP_DELTA_SECRETKEY');
        $login = env('PTOP_DELTA_LOGIN');
        $seed = date('c');
        if (function_exists('random_bytes')) {
            $nonce = bin2hex(random_bytes(16));
        } elseif (function_exists('openssl_random_pseudo_bytes')) {
            $nonce = bin2hex(openssl_random_pseudo_bytes(16));
        } else {
            $nonce = mt_rand();
        }
        $nonceBase64 = base64_encode($nonce);
        $tranKey = base64_encode(sha1($nonce . $seed . $secretKey, true));

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => env('PLACETOPAY_URL')."api/session/".$_POST['requestId'],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/json",
                "Postman-Token: 96983e26-ba1b-4dfa-bbe9-54f877428d08"
            ),
        ));
        $request = [
            "auth"=>[
                "login"=> $login,
                "seed"=> $seed,
                "nonce"=> $nonceBase64,
                "tranKey"=> $tranKey
            ]
        ];

        $data = json_encode($request);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            return $err;
        } else {
            return $response;
        }
    }

    public function pagPruebas(Request $request)
    {

        $secretKey = env('PTOP_DELTA_SECRETKEY');
        $login = env('PTOP_DELTA_LOGIN');

        //CREAMOS DATOS PARA LA AUTENTIFICACION EN PLACE TO PAY
        $seed = date('c');
        if (function_exists('random_bytes')) {
            $nonce = bin2hex(random_bytes(16));
        } elseif (function_exists('openssl_random_pseudo_bytes')) {
            $nonce = bin2hex(openssl_random_pseudo_bytes(16));
        } else {
            $nonce = mt_rand();
        }
        $nonceBase64 = base64_encode($nonce);
        $tranKey = base64_encode(sha1($nonce . $seed . $_POST['secretKey'], true));
        $request = [
            "auth" => [
                "login" => $_POST['login'],
                "seed" => $seed,
                "nonce" => $nonceBase64,
                "tranKey" => $tranKey
            ],
            "locale" => $_POST['locale'],
            "comprador" => [
                "documentType" => $_POST['CdocumentType'],
                "document" => $_POST['Cdocument'],
                "name" => $_POST['Cname'],
                "surname" => $_POST['Csurname'],
                "company" => $_POST['Ccompany'],
                "email" => $_POST['Cemail'],
                "address" => [
                    "street" => $_POST['Cstreet'],
                    "city" => $_POST['Ccity'],
                    "state" => $_POST['Cstate'],
                    "postalCode" => $_POST['CpostalCode'],
                    "country" => $_POST['Ccountry'],
                    "phone" => $_POST['Cphone']
                ],
                "mobile" => $_POST['Cmobile']
            ],
            "pagador" => [
                "documentType" => $_POST['PdocumentType'],
                "document" => $_POST['Pdocument'],
                "name" => $_POST['Pname'],
                "surname" => $_POST['Psurname'],
                "company" => $_POST['Pcompany'],
                "email" => $_POST['Pemail'],
                "address" => [
                    "street" => $_POST['Pstreet'],
                    "city" => $_POST['Pcity'],
                    "state" => $_POST['Pstate'],
                    "postalCode" => $_POST['PpostalCode'],
                    "country" => $_POST['Pcountry'],
                    "phone" => $_POST['Pphone']
                ],
                "mobile" => $_POST['Pmobile']
            ],
            "payment" => [
                "reference" => $_POST['reference'],
                "description" => $_POST['description'],
                "amount" => [
                    "currency" => $_POST['currency'],
                    "total" => $_POST['total'],
                    "taxes" => [
                        [
                            "kind" => $_POST['tkind1'],
                            "amount" => $_POST['tamount1'],
                            "base"=> $_POST['tbase1']
                        ],
                        [
                            "kind" => $_POST['tkind2'],
                            "amount" => $_POST['tamount2'],
                            "base"=> $_POST['tbase2']
                        ]
                    ],
                    "details" => [
                        [
                            "kind" => $_POST['dkind1'],
                            "amount" => $_POST['damount1']
                        ],
                        [
                            "kind" => $_POST['dkind2'],
                            "amount" => $_POST['damount2']
                        ],
                        [
                            "kind" => $_POST['dkind3'],
                            "amount" => $_POST['damount3']
                        ]
                    ]
                ]
            ],
            "items" => [
                [
                    "sku" => $_POST['isku'],
                    "name" => $_POST['iname'],
                    "category" => $_POST['icategory'],
                    "qty" => $_POST['iqty'],
                    "price" => $_POST['iprice'],
                    "tax" => $_POST['itax']
                ]
            ],
            "allowPartial" => $_POST['allowPartial'],
            "expiration" => $_POST['expiration'],
            //"expiration" => date('c', strtotime('+1 hour')),
            "returnUrl" => url('/pruebas/return/placetopay'),
            "cancelUrl" => url('/pruebas/cancel/placetopay'),
            "ipAddress" => \request()->ip(),
            "userAgent" => \request()->header('User-Agent'),
            "userAgent" => "SAFARI",
            "paymentMethod" => $_POST['paymentMethod'],
            "skipResult" => $_POST['skipResult'],
            "noBuyerFill" => $_POST['noBuyerFill']
        ];
        $data = json_encode($request);
       // dd($data);

        //SOLICITAMOS LA CREACION DE SESSION PARA EL PAGO PLACETOPAY
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => env('PLACETOPAY_URL')."api/session",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/json",
                "Postman-Token: 96983e26-ba1b-4dfa-bbe9-54f877428d08"
            ),
        ));
//        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
//        dd($response);
        if ($err) {
            return $err;
        } else {
            $resArray = json_decode($response,true);
            return $resArray;
        }
    }
}
