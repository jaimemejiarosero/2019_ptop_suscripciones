<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models;

class actpagosController extends Controller
{
    public function pendientes()
    {

        $aluPagos = Models\actalupagos
            ::join('actpagos', 'actalupagos.requestId', '=', 'actpagos.requestId')
            ->join('acmalumna', 'actalupagos.alu_codalum', '=', 'acmalumna.alu_codalum')
            ->where('actalupagos.status', '=', 'P')
            ->select('actpagos.requestId','actpagos.url','acmalumna.alu_colegio')
            ->distinct()
            ->get();

        if ($aluPagos->count() > 0) {
            $seed = date('c');
            if (function_exists('random_bytes')) {
                $nonce = bin2hex(random_bytes(16));
            } elseif (function_exists('openssl_random_pseudo_bytes')) {
                $nonce = bin2hex(openssl_random_pseudo_bytes(16));
            } else {
                $nonce = mt_rand();
            }
            $nonce = "7fcfc4f713663bb9ae401e26ad7da0b2";
            $nonceBase64 = base64_encode($nonce);


            foreach ($aluPagos as $pagPendientes) {

                if($pagPendientes->getAttributes()['alu_colegio'] == 'T') {
                    $secretKey = env('PTOP_TORREMAR_SECRETKEY');
                    $login = env('PTOP_TORREMAR_LOGIN');
                }else{
                    $secretKey = env('PTOP_DELTA_SECRETKEY');
                    $login = env('PTOP_DELTA_LOGIN');
                }

                $tranKey = base64_encode(sha1($nonce . $seed . $secretKey, true));

                //SOLICITAMOS LA CREACION DE SESSION PARA EL PAGO PLACETOPAY
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => env('PLACETOPAY_URL') . "api/session/" . $pagPendientes->getAttributes()['requestId'],
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_HTTPHEADER => array(
                        "Cache-Control: no-cache",
                        "Content-Type: application/json",
                        "Postman-Token: 96983e26-ba1b-4dfa-bbe9-54f877428d08"
                    ),
                ));
                $request = [
                    "auth" => [
                        "login" => $login,
                        "seed" => $seed,
                        "nonce" => $nonceBase64,
                        "tranKey" => $tranKey
                    ]
                ];
                $data = json_encode($request);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

                $response = curl_exec($curl);
                $err = curl_error($curl);

                curl_close($curl);
                //var_dump($response);


                if ($err) {
                    return $err;
                } else {

                    $resArray = json_decode($response, true);
                    if ($resArray['status']['status'] == 'PENDING' ) {
                        $url = $pagPendientes->getAttributes()['url'];
                        $requestId = $pagPendientes->getAttributes()['requestId'];
                    } else {
                        if ($resArray['status']['status'] == 'APPROVED') {
                            $alupagos = Models\actalupagos::leftJoin('actpagos', 'actpagos.requestId', '=', 'actalupagos.requestId')
                                ->where('actpagos.requestId', '=', $pagPendientes->getAttributes()['requestId'])
                                ->update(['actalupagos.status' => 'A']);

                            Models\actpagos::where('requestId', $pagPendientes->getAttributes()['requestId'])
                                ->update(['status' => 'APPROVED',
                                    'message' => 'La petición se ha procesado correctamente',
                                    'detalle' => $resArray['payment'][0]['status']['message'],
                                    'date' => date("Y-m-d H:i:s")]);

                            $alumna = Models\actalupagos::join('acmalumna', 'acmalumna.alu_codalum', '=', 'actalupagos.alu_codalum')
                                ->where('actalupagos.requestId', '=', $pagPendientes->getAttributes()['requestId'])
                                ->update(['acmalumna.alu_estadomatriculado' => 'S', 'acmalumna.alu_fechaactualiza' => date("Y-m-d H:i:s")]);

                        } else {
                            $alupagos = Models\actalupagos::leftJoin('actpagos', 'actpagos.requestId', '=', 'actalupagos.requestId')
                                ->where('actpagos.requestId', '=', $pagPendientes->getAttributes()['requestId'])
                                ->update(['actalupagos.status' => 'R']);

                            Models\actpagos::where('requestId', $pagPendientes->getAttributes()['requestId'])
                                ->update(['status' => $resArray['status']['status'],
                                    'message' => $resArray['status']['message'],
                                    'detalle' => '',
                                    'date' => date("Y-m-d H:i:s")]);
                        }
                    }
                }
            }
        }
        dd($resArray);
    }

}