<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models;
use Illuminate\Support\Facades\Hash;
use App;


class acmfamiliController extends Controller
{
    public function index(Request $request, $fam_secuencia, $logeado)
    {
        //echo $request->session()->get('user');
        //echo $fam_secuencia;
        if (($logeado === base64_encode($fam_secuencia)) and ($request->session()->get('user') == $fam_secuencia)) {
            $urldelta='';
            $urltorremar='';
            $requestId='';
            $alumnasDelta='';
            $alumnasTorremar='';
            $famili = Models\acmfamili::where('fam_secuencia', $fam_secuencia)->get();
            $suscripciones = Models\suscripciones::where('fam_secuencia', $fam_secuencia)->get();
            //$alumnas = Models\acmalumna::where('fam_secuencia', $fam_secuencia)->get();

            $aluPagosDelta   = Models\actalupagos
                ::join('actpagos', 'actalupagos.requestId', '=', 'actpagos.requestId')
                ->join('acmalumna', 'actalupagos.alu_codalum', '=', 'acmalumna.alu_codalum')
                ->select('actpagos.*','acmalumna.*')
                ->where('acmalumna.fam_secuencia','=',$fam_secuencia)
                ->where('actalupagos.status','=','P')
                ->whereIn('acmalumna.alu_colegio', array('D', 'P'))
                ->get();
            $aluPagosTorremar   = Models\actalupagos
                ::join('actpagos', 'actalupagos.requestId', '=', 'actpagos.requestId')
                ->join('acmalumna', 'actalupagos.alu_codalum', '=', 'acmalumna.alu_codalum')
                ->select('actpagos.*','acmalumna.*')
                ->where('acmalumna.fam_secuencia','=',$fam_secuencia)
                ->where('acmalumna.alu_colegio','=','T')
                ->where('actalupagos.status','=','P')
                ->get();

            //var_dump($aluPagos);
            if($aluPagosDelta->count() > 0) {

                $seed = date('c');
                if (function_exists('random_bytes')) {
                    $nonce = bin2hex(random_bytes(16));
                } elseif (function_exists('openssl_random_pseudo_bytes')) {
                    $nonce = bin2hex(openssl_random_pseudo_bytes(16));
                } else {
                    $nonce = mt_rand();
                }

                $nonceBase64 = base64_encode($nonce);

                $secretKey = env('PTOP_DELTA_SECRETKEY');
                $login = env('PTOP_DELTA_LOGIN');

                $tranKey = base64_encode(sha1($nonce . $seed . $secretKey, true));

                foreach ($aluPagosDelta as $pagPendientes) {

                    //SOLICITAMOS LA CREACION DE SESSION PARA EL PAGO PLACETOPAY
                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => env('PLACETOPAY_URL')."api/session/".$pagPendientes->getAttributes()['requestId'],
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "POST",
                        CURLOPT_HTTPHEADER => array(
                            "Cache-Control: no-cache",
                            "Content-Type: application/json",
                            "Postman-Token: 96983e26-ba1b-4dfa-bbe9-54f877428d08"
                        ),
                    ));
                    $request = [
                        "auth"=>[
                            "login"=> $login,
                            "seed"=> $seed,
                            "nonce"=> $nonceBase64,
                            "tranKey"=> $tranKey
                        ]
                    ];
                    $data = json_encode($request);
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

                    $response = curl_exec($curl);
                    $err = curl_error($curl);

                    curl_close($curl);
                    //var_dump($response);


                    if ($err) {
                        return $err;
                    } else {

                        $resArray = json_decode($response,true);
                        if($resArray['status']['status'] == 'PENDING'){
                            $urldelta= $pagPendientes->getAttributes()['url'];
                            $requestId= $pagPendientes->getAttributes()['requestId'];
                        }else{
                            if($resArray['status']['status'] == 'APPROVED') {
                                Models\actalupagos::leftJoin('actpagos', 'actpagos.requestId', '=', 'actalupagos.requestId')
                                    ->where('actpagos.requestId', '=', $pagPendientes->getAttributes()['requestId'])
                                    ->update(['actalupagos.status' => 'A']);

                                Models\actpagos::where('requestId', $pagPendientes->getAttributes()['requestId'])
                                    ->update(['status' => 'APPROVED',
                                              'message'=>'La petición se ha procesado correctamente',
                                              'detalle'=>$resArray['payment'][0]['status']['message'],
                                              'date'=>date("Y-m-d H:i:s")]);

                                Models\actalupagos::join('acmalumna', 'acmalumna.alu_codalum', '=', 'actalupagos.alu_codalum')
                                    ->where('actalupagos.requestId', '=', $pagPendientes->getAttributes()['requestId'])
                                    ->update(['acmalumna.alu_estadomatriculado'=>'S','acmalumna.alu_fechaactualiza'=>date("Y-m-d H:i:s")]);

                            }else{
                                Models\actalupagos::leftJoin('actpagos', 'actpagos.requestId', '=', 'actalupagos.requestId')
                                    ->where('actpagos.requestId', '=', $pagPendientes->getAttributes()['requestId'])
                                    ->update(['actalupagos.status'=>'R']);

                                Models\actpagos::where('requestId',$pagPendientes->getAttributes()['requestId'])
                                    ->update(['status'=>'REJECTED',
                                        'message'=>'La petición ha sido rechazada',
                                        'detalle'=>$resArray['status']['message'],
                                        'date'=>date("Y-m-d H:i:s")]);
                            }
                            $urldelta = '';
                            $requestId = '';
                        }
                        $alumnasDelta = Models\acmalumna::join('tb_curso', 'tb_curso.codnivel', '=', 'acmalumna.alu_cursomatricular')
                            ->where('acmalumna.fam_secuencia', '=', $fam_secuencia)
                            ->whereIn('acmalumna.alu_colegio', array('D', 'P'))
                            ->get();


                    }

                    //var_dump($pagPendientes->getAttributes()['requestId']);
                }
            }else{

                $alumnasDelta = Models\acmalumna::join('tb_curso', 'tb_curso.codnivel', '=', 'acmalumna.alu_cursomatricular')
                    ->where('acmalumna.fam_secuencia', '=', $fam_secuencia)
                    ->whereIn('acmalumna.alu_colegio', array('D', 'P'))
                    ->get();


            }
            //TORREMAR
            if($aluPagosTorremar->count() > 0) {

                $seed = date('c');
                if (function_exists('random_bytes')) {
                    $nonce = bin2hex(random_bytes(16));
                } elseif (function_exists('openssl_random_pseudo_bytes')) {
                    $nonce = bin2hex(openssl_random_pseudo_bytes(16));
                } else {
                    $nonce = mt_rand();
                }

                $nonceBase64 = base64_encode($nonce);

                $secretKey = env('PTOP_TORREMAR_SECRETKEY');
                $login = env('PTOP_TORREMAR_LOGIN');

                $tranKey = base64_encode(sha1($nonce . $seed . $secretKey, true));

                foreach ($aluPagosTorremar as $pagPendientes) {

                    //SOLICITAMOS LA CREACION DE SESSION PARA EL PAGO PLACETOPAY
                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => env('PLACETOPAY_URL')."api/session/".$pagPendientes->getAttributes()['requestId'],
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "POST",
                        CURLOPT_HTTPHEADER => array(
                            "Cache-Control: no-cache",
                            "Content-Type: application/json",
                            "Postman-Token: 96983e26-ba1b-4dfa-bbe9-54f877428d08"
                        ),
                    ));
                    $request = [
                        "auth"=>[
                            "login"=> $login,
                            "seed"=> $seed,
                            "nonce"=> $nonceBase64,
                            "tranKey"=> $tranKey
                        ]
                    ];
                    $data = json_encode($request);
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

                    $response = curl_exec($curl);
                    $err = curl_error($curl);

                    curl_close($curl);
                    //var_dump($response);


                    if ($err) {
                        return $err;
                    } else {

                        $resArray = json_decode($response,true);
                        if($resArray['status']['status'] == 'PENDING'){
                            $urltorremar= $pagPendientes->getAttributes()['url'];
                            $requestId= $pagPendientes->getAttributes()['requestId'];
                        }else{
                            if($resArray['status']['status'] == 'APPROVED') {
                                Models\actalupagos::leftJoin('actpagos', 'actpagos.requestId', '=', 'actalupagos.requestId')
                                    ->where('actpagos.requestId', '=', $pagPendientes->getAttributes()['requestId'])
                                    ->update(['actalupagos.status' => 'A']);

                                Models\actpagos::where('requestId', $pagPendientes->getAttributes()['requestId'])
                                    ->update(['status' => 'APPROVED',
                                              'message'=>'La petición se ha procesado correctamente',
                                              'detalle'=>$resArray['payment'][0]['status']['message'],
                                              'date'=>date("Y-m-d H:i:s")]);

                                Models\actalupagos::join('acmalumna', 'acmalumna.alu_codalum', '=', 'actalupagos.alu_codalum')
                                    ->where('actalupagos.requestId', '=', $pagPendientes->getAttributes()['requestId'])
                                    ->update(['acmalumna.alu_estadomatriculado'=>'S','acmalumna.alu_fechaactualiza'=>date("Y-m-d H:i:s")]);

                            }else{
                                Models\actalupagos::leftJoin('actpagos', 'actpagos.requestId', '=', 'actalupagos.requestId')
                                    ->where('actpagos.requestId', '=', $pagPendientes->getAttributes()['requestId'])
                                    ->update(['actalupagos.status'=>'R']);

                                Models\actpagos::where('requestId',$pagPendientes->getAttributes()['requestId'])
                                    ->update(['status'=>'REJECTED',
                                        'message'=>'La petición ha sido rechazada',
                                        'detalle'=>$resArray['payment'][0]['status']['message'],
                                        'date'=>date("Y-m-d H:i:s")]);
                            }
                            $urltorremar = '';
                            $requestId = '';
                        }
                        $alumnasTorremar = Models\acmalumna::join('tb_curso', 'tb_curso.codnivel', '=', 'acmalumna.alu_cursomatricular')
                            ->where('acmalumna.fam_secuencia', '=', $fam_secuencia)
                            ->where('acmalumna.alu_colegio', '=', 'T')
                            ->get();
                    }

                    //var_dump($pagPendientes->getAttributes()['requestId']);
                }
            }else{

                $alumnasTorremar = Models\acmalumna::join('tb_curso', 'tb_curso.codnivel', '=', 'acmalumna.alu_cursomatricular')
                    ->where('acmalumna.fam_secuencia', '=', $fam_secuencia)
                    ->where('acmalumna.alu_colegio', '=', 'T')
                    ->get();


            }


                return view('pagos', [
                    'famili' => $famili,
                    'suscripciones' => $suscripciones,
                    'alumnasDelta' => $alumnasDelta,
                    'alumnasTorremar' => $alumnasTorremar,
                    'urldelta'=>$urldelta,
                    'urltorremar'=>$urltorremar,
                    'requestId'=>$requestId,
                    'aluPagosDelta' => $aluPagosDelta,
                    'aluPagosTorremar' => $aluPagosTorremar,

                ]);

        }else{
                return view('login');
        }
    }

    public function login(Request $request){
        if ($request->session()->get('user') != '') {
            if ($request->session()->get('user') != '15') {
                return redirect()->action('acmfamiliController@index', [$request->session()->get('user'), base64_encode($request->session()->get('user'))]);
            }else{
                return redirect()->action('suscripcionController@index');
            }
        }

        return view('login');
    }

    public function logout(Request $request){
        $request->session()->put('user','');

        return redirect()->action('acmfamiliController@login');
    }

    public function valUsuario(Request $request)
    {
        $data = $request->request->all();
        $clave = $data['clave'];
        $message = '';
        //$hashed = Hash::make($clave);
        $usuario = $data['usuario'];

        $famili = Models\acmfamili::where('fam_user', $usuario)
            ->where('fam_passw', $clave)
            ->get();
        if(count($famili) > 0){
            $request->session()->put('user',$famili[0]['fam_secuencia']);
            $message = "/".$famili[0]['fam_secuencia']."/".base64_encode($famili[0]['fam_secuencia']);
        }else{
            $message =  "Usuario o Contraseña no válidas" ;
        }
        return \GuzzleHttp\json_encode($message, true);
    }


    public function historial(Request $request){

        if ($request->session()->has('user')) {
            $fam_secuencia = $request->session()->get('user');
            $famili = Models\acmfamili::where('fam_secuencia', $fam_secuencia)->get();
            $pagosRegistrados = Models\actalupagos
                ::join('actpagos', 'actalupagos.requestId', '=', 'actpagos.requestId')
                ->join('acmalumna', 'actalupagos.alu_codalum', '=', 'acmalumna.alu_codalum')
                ->where('acmalumna.fam_secuencia','=',$fam_secuencia)
                ->select('actpagos.requestId','actpagos.message','actpagos.reference','actpagos.date','actpagos.monto')
                ->distinct()
                ->get();

            return view('historial', [
                'pagosRegistrados' => $pagosRegistrados,
                'famili'=>$famili,
            ]);

        }

        return view('login');
    }


    public function perfil(Request $request){

        if ($request->session()->has('user')) {
            $fam_secuencia = $request->session()->get('user');

            $famili = Models\acmfamili::where('fam_secuencia', $fam_secuencia)->get();


            return view('perfil', [
                'famili' => $famili,
            ]);

        }

        return view('login');
    }

    public function perGuardar(Request $request){

        if ($request->session()->has('user')) {
            include('mobile.php');
            $data = $request->all();

            $fam_secuencia = $request->session()->get('user');

            $familiG = Models\acmfamili::where('fam_secuencia', $fam_secuencia)
                ->update(['fam_nompadr'=>$data['nombrespa'],'fam_apepadr'=>$data['apellidospa'],'fam_cedupa'=>$data['cedulapa'],'fam_dirpadr'=>$data['direccionpa'],'fam_telpadr'=>$data['telefonopa'],'fam_email00'=>$data['emailpa'],'fam_trapadr'=>$data['trabajopa'],'fam_carpadr'=>$data['cargopa'],'fam_titpadr'=>$data['titulopa'],'fam_dirofip'=>$data['dirofipa'],
                          'fam_nommadr'=>$data['nombresma'],'fam_apematr'=>$data['apellidosma'],'fam_ceduma'=>$data['cedulama'],'fam_dirmadr'=>$data['direccionma'],'fam_telmadr'=>$data['telefonoma'],'fam_emailma'=>$data['emailma'],'fam_tramadr'=>$data['trabajoma'],'fam_carmadr'=>$data['cargoma'],'fam_titmadr'=>$data['tituloma'],'fam_dirofim'=>$data['dirofima'],]);

            $famili = Models\acmfamili::where('fam_secuencia', $fam_secuencia)->get();

            return redirect()->action('acmfamiliController@index', [$request->session()->get('user'),base64_encode($request->session()->get('user'))]);


        }

        return view('login');
    }

    public function faq(Request $request){

        if ($request->session()->has('user')) {
            $fam_secuencia = $request->session()->get('user');
            $famili = Models\acmfamili::where('fam_secuencia', $fam_secuencia)->get();
            return view('faq',[
                'famili'=>$famili,
            ]);
        }

        return view('login');
    }

    public function enviar_clave(Request $request){

            return view('enviar_clave');
    }

    public function valEnvio(Request $request)
    {
        $data = $request->request->all();
        $famili = Models\acmfamili::where('fam_cedupa', $data['cedula'])
            ->orWhere('fam_ceduma',$data['cedula'])->get();
        if($famili->count() > 0){
            $message = "Su contraseña sera enviada al correo electrónico registrado";
        }else{
            $message = "Número de Cédula No encontrado";
        }

    return \GuzzleHttp\json_encode($message, true);

    }

    public function terminos(Request $request)
    {
        if ($request->session()->has('user')) {
            $fam_secuencia = $request->session()->get('user');
            $famili = Models\acmfamili::where('fam_secuencia', $fam_secuencia)->get();
            return view('terminos_condiciones',[
                'famili'=>$famili
            ]);
        }

        return view('login');
    }

}
