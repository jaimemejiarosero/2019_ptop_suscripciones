<?php

namespace App\Http\Controllers;

use App\Models;
use Composer\Command\UpdateCommand;
use Illuminate\Http\Request;
use App;

class suscripcionController extends Controller
{
    public function verCobros(Request $request, $fam_secuencia)
    {
        if ($fam_secuencia != '') {
            $urldelta='';
            $urltorremar='';
            $requestId='';
            $alumnasDelta='';
            $alumnasTorremar='';
            $famili = Models\acmfamili::where('fam_secuencia', $fam_secuencia)->get();
            $suscripcion = Models\suscripciones::where('fam_secuencia', $fam_secuencia)->get();
            //$alumnas = Models\acmalumna::where('fam_secuencia', $fam_secuencia)->get();

            $aluPagosDelta   = Models\actalupagos
                ::join('actpagos', 'actalupagos.requestId', '=', 'actpagos.requestId')
                ->join('acmalumna', 'actalupagos.alu_codalum', '=', 'acmalumna.alu_codalum')
                ->select('actpagos.*','acmalumna.*')
                ->where('acmalumna.fam_secuencia','=',$fam_secuencia)
                ->where('actalupagos.status','=','P')
                ->whereIn('acmalumna.alu_colegio', array('D', 'P'))
                ->get();

            //var_dump($aluPagos);
            if($aluPagosDelta->count() > 0) {

                $seed = date('c');
                if (function_exists('random_bytes')) {
                    $nonce = bin2hex(random_bytes(16));
                } elseif (function_exists('openssl_random_pseudo_bytes')) {
                    $nonce = bin2hex(openssl_random_pseudo_bytes(16));
                } else {
                    $nonce = mt_rand();
                }

                $nonceBase64 = base64_encode($nonce);

                $secretKey = env('PTOP_DELTA_SECRETKEY');
                $login = env('PTOP_DELTA_LOGIN');

                $tranKey = base64_encode(sha1($nonce . $seed . $secretKey, true));

                foreach ($aluPagosDelta as $pagPendientes) {

                    //SOLICITAMOS LA CREACION DE SESSION PARA EL PAGO PLACETOPAY
                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => env('PLACETOPAY_URL')."api/session/".$pagPendientes->getAttributes()['requestId'],
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "POST",
                        CURLOPT_HTTPHEADER => array(
                            "Cache-Control: no-cache",
                            "Content-Type: application/json",
                            "Postman-Token: 96983e26-ba1b-4dfa-bbe9-54f877428d08"
                        ),
                    ));
                    $request = [
                        "auth"=>[
                            "login"=> $login,
                            "seed"=> $seed,
                            "nonce"=> $nonceBase64,
                            "tranKey"=> $tranKey
                        ]
                    ];
                    $data = json_encode($request);
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

                    $response = curl_exec($curl);
                    $err = curl_error($curl);

                    curl_close($curl);
                    //var_dump($response);


                    if ($err) {
                        return $err;
                    } else {

                        $resArray = json_decode($response,true);
                        if($resArray['status']['status'] == 'PENDING'){
                            $urldelta= $pagPendientes->getAttributes()['url'];
                            $requestId= $pagPendientes->getAttributes()['requestId'];
                        }else{
                            if($resArray['status']['status'] == 'APPROVED') {
                                Models\actalupagos::leftJoin('actpagos', 'actpagos.requestId', '=', 'actalupagos.requestId')
                                    ->where('actpagos.requestId', '=', $pagPendientes->getAttributes()['requestId'])
                                    ->update(['actalupagos.status' => 'A']);

                                Models\actpagos::where('requestId', $pagPendientes->getAttributes()['requestId'])
                                    ->update(['status' => 'APPROVED',
                                        'message'=>'La petición se ha procesado correctamente',
                                        'detalle'=>$resArray['payment'][0]['status']['message'],
                                        'date'=>date("Y-m-d H:i:s")]);

                                Models\actalupagos::join('acmalumna', 'acmalumna.alu_codalum', '=', 'actalupagos.alu_codalum')
                                    ->where('actalupagos.requestId', '=', $pagPendientes->getAttributes()['requestId'])
                                    ->update(['acmalumna.alu_estadomatriculado'=>'S','acmalumna.alu_fechaactualiza'=>date("Y-m-d H:i:s")]);

                            }else{
                                Models\actalupagos::leftJoin('actpagos', 'actpagos.requestId', '=', 'actalupagos.requestId')
                                    ->where('actpagos.requestId', '=', $pagPendientes->getAttributes()['requestId'])
                                    ->update(['actalupagos.status'=>'R']);

                                Models\actpagos::where('requestId',$pagPendientes->getAttributes()['requestId'])
                                    ->update(['status'=>'REJECTED',
                                        'message'=>'La petición ha sido rechazada',
                                        'detalle'=>$resArray['status']['message'],
                                        'date'=>date("Y-m-d H:i:s")]);
                            }
                            $urldelta = '';
                            $requestId = '';
                        }
                        $alumnasDelta = Models\acmalumna::join('tb_curso', 'tb_curso.codnivel', '=', 'acmalumna.alu_cursomatricular')
                            ->where('acmalumna.fam_secuencia', '=', $fam_secuencia)
                            ->whereIn('acmalumna.alu_colegio', array('D', 'P'))
                            ->get();


                    }

                    //var_dump($pagPendientes->getAttributes()['requestId']);
                }
            }else{

                $alumnasDelta = Models\acmalumna::join('tb_curso', 'tb_curso.codnivel', '=', 'acmalumna.alu_cursomatricular')
                    ->where('acmalumna.fam_secuencia', '=', $fam_secuencia)
                    ->whereIn('acmalumna.alu_colegio', array('D', 'P'))
                    ->get();


            }
            return view('cobros', [
                'famili' => $famili,
                'suscripcion' => $suscripcion[0],
                'alumnasDelta' => $alumnasDelta,
                'urldelta'=>$urldelta,
                'requestId'=>$requestId,
                'aluPagosDelta' => $aluPagosDelta,
            ]);

        }else{
            return view('login');
        }
    }

    public function index(Request $request)
    {
        if ($request->session()->has('user')) {
            $fam_secuencia = $request->session()->get('user');
            $suscripciones = Models\suscripciones::all();
            return view('suscripciones',[
                'suscripciones'=>$suscripciones,
            ]);
        }

        return view('login');
    }

    public function suscripcion(Request $request)
    {
        $secretKey = env('PTOP_DELTA_SECRETKEY');
        $login = env('PTOP_DELTA_LOGIN');

        //CREAMOS DATOS PARA LA AUTENTIFICACION EN PLACE TO PAY
        $seed = date('c');
        if (function_exists('random_bytes')) {
            $nonce = bin2hex(random_bytes(16));
        } elseif (function_exists('openssl_random_pseudo_bytes')) {
            $nonce = bin2hex(openssl_random_pseudo_bytes(16));
        } else {
            $nonce = mt_rand();
        }
        $nonceBase64 = base64_encode($nonce);
        $tranKey = base64_encode(sha1($nonce . $seed . $secretKey, true));

        $fam_secuencia = $request->session()->get('user');

        $datComprador = Models\acmfamili::where('fam_secuencia', '=', $fam_secuencia)->get();
            if ($datComprador[0]['fam_tipo_docpdr'] == 'C') {
                $docType = "CI";
            } else {
                if ($datComprador[0]['fam_tip_docpdr'] == 'R') {
                    $docType = "NIT";
                } else {
                    $docType = "CI";
                }
            }
            $doc = $datComprador[0]['fam_cedupa'];
            $nombres = $datComprador[0]['fam_nompadr'];
            $apellidos = $datComprador[0]['fam_apepadr'];
            $email = $datComprador[0]['fam_email00'];
            $street = $datComprador[0]['fam_dirpadr'];
            $city = "";
            $state = "";
            $postalCode = "";
            $country = "EC";
            $phone = substr($datComprador[0]['fam_telpadr'],0,30);
            $mobile = substr($datComprador[0]['fam_telmovp'],0,30);

        $pagador = [
            "documentType" => $docType,
            "document" => $doc,
            "name" => $nombres,
            "surname" => $apellidos,
            "company" => null,
            "email" => 'jaime.mejia@ppm.com.ec',
            "address" => [
                "street" => $street,
                "city" => $city,
                "state" => $state,
                "postalCode" => $postalCode,
                "country" => $country,
                "phone" => $phone
            ],
            "mobile" => $mobile
        ];

        //SOLICITAMOS LA CREACION DE SESSION PARA EL PAGO PLACETOPAY
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => env('PLACETOPAY_URL')."api/session",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/json",
                "Postman-Token: 96983e26-ba1b-4dfa-bbe9-54f877428d08"
            ),
        ));


            $reference = 'SUS_' . time();
            $request = [
                "auth"=>[
                    "login"=> $login,
                    "seed"=> $seed,
                    "nonce"=> $nonceBase64,
                    "tranKey"=> $tranKey
                ],
                "buyer" => $pagador,
                "subscription" => [
                    "reference"=> $reference,
                    "description" => "Una suscripción de prueba"
                ],

                "expiration"=> date('c', strtotime('+1 hour')),
                "returnUrl" =>  url('/pagos/okplacetopay'),
                "ipAddress" => \request()->ip(),
                "userAgent" => \request()->header('User-Agent'),
                "skipResult"=> true,
                "fields"=> [
                    [
                        "keyword"=> "familia",
                        "value"=> $fam_secuencia,
                        "displayOn"=> "payment"
                    ]
                ]
            ];


        $data = json_encode($request);

        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        $response = \GuzzleHttp\json_decode($response,true);
        //dd($response['status']);
        if ($err) {
            return $err;
        } else {
            return $response;
        }
    }



    public function susCobros(){
        //CREAMOS DATOS PARA LA AUTENTIFICACION EN PLACE TO PAY
        $seed = date('c');
        if (function_exists('random_bytes')) {
            $nonce = bin2hex(random_bytes(16));
        } elseif (function_exists('openssl_random_pseudo_bytes')) {
            $nonce = bin2hex(openssl_random_pseudo_bytes(16));
        } else {
            $nonce = mt_rand();
        }

        $nonceBase64 = base64_encode($nonce);
        $secretKey = env('PTOP_DELTA_SECRETKEY');
        $login = env('PTOP_DELTA_LOGIN');


        $tranKey = base64_encode(sha1($nonce . $seed . $secretKey, true));
        $totPorPagar = 395.50;
        $fam_secuencia = $_POST['fam_secuencia'];

        $alumno = $_POST['alu_codigo'];
        $pagador = [
            "documentType" => $_POST['documentType'],
            "document" => $_POST['document'],
            "name" => $_POST['name'],
            "surname" => $_POST['surname'],
            "email" => $_POST['email'],
            "mobile" => $_POST['mobile'],
        ];
        $instrument = [
            "token" => [
                "token" => $_POST['token'],
            ],
        ];

        $reference = 'COB_' . time();

        $request = [
            "auth" => [
                "login" => $login,
                "seed" => $seed,
                "nonce" => $nonceBase64,
                "tranKey" => $tranKey
            ],
            "instrument" => $instrument,
            "locale" => 'es_EC',
            "payer" => $pagador,
            "payment" => [
                "reference" => $reference,
                "description" => "Cobro por suscripción",
                "amount" => [
                    "currency" => "USD",
                    "total" => round($totPorPagar, 2),
                    "taxes" => null,
                ],
                "items" => [
                    [
                        "sku" => "COPECE",
                        "name" => "Matrícula",
                        "category" => "digital",
                        "qty" => 1,
                        "price" => round($totPorPagar, 2),
                        "tax" => null
                    ]
                ]
            ]
        ];
        $data = json_encode($request);
        //dd($data);
        //SOLICITAMOS LA CREACION DE SESSION PARA EL PAGO PLACETOPAY

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://test.placetopay.ec/redirection/api/collect",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/json",
                "Postman-Token: 96983e26-ba1b-4dfa-bbe9-54f877428d08"
            ),
            CURLOPT_USERAGENT => \request()->header('User-Agent'),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        if ($err) {
            return $err;
        } else {
            $resArray = json_decode($response,true);
            if($resArray['status']['status'] == 'FAILED'){
                return redirect()->action('suscripcionController@verCobros', [$fam_secuencia]);
            }
                //creamos el registro que relaciona pagos con alumnos y familiares
                $pagAlu = new Models\actalupagos();
                $pagAlu->alu_codalum = $alumno;
                $pagAlu->requestId = $resArray['requestId'];
                if($resArray['status']['status'] == 'APPROVED'){
                    $pagAlu->status = 'A';
                    $alumnos = Models\acmalumna::where('alu_codalum', '=', $alumno)
                        ->Where('alu_colegio','=','D')
                        ->Update(['alu_estadomatriculado'=>'S']);
                }else {
                    if ($resArray['status']['status'] == 'REJECTED') {
                        $pagAlu->status = 'R';
                    }else{
                        $pagAlu->status = 'P';
                    }
                }
                $pagAlu->save();
                //creamos el registro del pago en la bd cliente
                $pago = new Models\actpagos();
                $pago->requestId = $resArray['requestId'];
                $pago->date = date("Y-m-d H:i:s");
                $pago->status = $resArray['status']['status'];
                $pago->message = $resArray['status']['message'];;
                $pago->reference = $reference;
                $pago->signature = '';
                $pago->url = '';
                $pago->monto = round($totPorPagar,2);
                $pago->tipo_transaccion = 'SUS';
                //if($resArray['status']['status'] != 'PENDIEN'){
                //    $pago->detalle = $resArray['payment']['status']['message'];
                //}
                $pago->save();

            return redirect()->action('suscripcionController@verCobros', [$fam_secuencia]);
        }
    }

    public function susCobroDolar($requestId){
        $datreq = \GuzzleHttp\json_decode($this->getRequestPtoP($requestId),true);

        //CREAMOS DATOS PARA LA AUTENTIFICACION EN PLACE TO PAY
        $seed = date('c');
        if (function_exists('random_bytes')) {
            $nonce = bin2hex(random_bytes(16));
        } elseif (function_exists('openssl_random_pseudo_bytes')) {
            $nonce = bin2hex(openssl_random_pseudo_bytes(16));
        } else {
            $nonce = mt_rand();
        }

        $nonceBase64 = base64_encode($nonce);
        $secretKey = env('PTOP_DELTA_SECRETKEY');
        $login = env('PTOP_DELTA_LOGIN');
        $pagador = [
            "documentType" => $datreq['request']['payer']['documentType'],
            "document" => $datreq['request']['payer']['document'],
            "name" => $datreq['request']['payer']['name'],
            "surname" => $datreq['request']['payer']['surname'],
            "email" => $datreq['request']['payer']['email'],
            "mobile" => $datreq['request']['payer']['mobile'],
        ];
        $tranKey = base64_encode(sha1($nonce . $seed . $secretKey, true));
        $totPorPagar = 1;

        $instrument = [
            "token" => [
                "token" => $datreq['subscription']['instrument'][0]['value'],
            ],
        ];

        $reference = 'COB_' . time();

        $request = [
            "auth" => [
                "login" => $login,
                "seed" => $seed,
                "nonce" => $nonceBase64,
                "tranKey" => $tranKey
            ],
            "payer"=>$pagador,
            "instrument" => $instrument,
            "locale" => 'es_EC',
            "payment" => [
                "reference" => $reference,
                "description" => "Cobro por suscripción un dolar",
                "amount" => [
                    "currency" => "USD",
                    "total" => round($totPorPagar, 2),
                    "taxes" => null,
                ]
            ]
        ];
        $data = json_encode($request);
        //dd($data);
        //SOLICITAMOS LA CREACION DE SESSION PARA EL PAGO PLACETOPAY

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://test.placetopay.ec/redirection/api/collect",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/json",
                "Postman-Token: 96983e26-ba1b-4dfa-bbe9-54f877428d08"
            ),
            CURLOPT_USERAGENT => \request()->header('User-Agent'),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        if ($err) {
            return $err;
        } else {
            $famili = Models\acmfamili::where('fam_secuencia', $datreq['request']['fields'][0]['value'])->get();
            $resArray = json_decode($response,true);
            if($resArray['status']['status'] == 'APPROVED'){
                $suscripcion = new Models\suscripciones();
                $suscripcion->requestId = $requestId;
                $suscripcion->type = $datreq['subscription']['type'];
                $suscripcion->status = $datreq['subscription']['status']['status'];
                $suscripcion->message = $datreq['subscription']['status']['message'];
                $suscripcion->date = date("Y-m-d H:i:s");
                $suscripcion->token = $datreq['subscription']['instrument'][0]['value'];
                $suscripcion->document = $datreq['request']['payer']['document'];
                $suscripcion->documentType = $datreq['request']['payer']['documentType'];
                $suscripcion->name = $datreq['request']['payer']['name'];
                $suscripcion->surname = $datreq['request']['payer']['surname'];
                $suscripcion->email = $datreq['request']['payer']['email'];
                $suscripcion->mobile = $datreq['request']['payer']['mobile'];
                $suscripcion->fam_secuencia = $datreq['request']['fields'][0]['value'];
                $suscripcion->fam_nombre = $famili[0]['fam_user'];
                $suscripcion->save();
                $mensaje = "Suscripción exitosa";
                //reversar transacción
                $pagoDolar = \GuzzleHttp\json_decode($this->getRequestPtoP($resArray['requestId']),true);
                $internal = $pagoDolar['payment'][0]['internalReference'];
                $seed = date('c');
                if (function_exists('random_bytes')) {
                    $nonce = bin2hex(random_bytes(16));
                } elseif (function_exists('openssl_random_pseudo_bytes')) {
                    $nonce = bin2hex(openssl_random_pseudo_bytes(16));
                } else {
                    $nonce = mt_rand();
                }

                $nonceBase64 = base64_encode($nonce);
                $secretKey = env('PTOP_DELTA_SECRETKEY');
                $login = env('PTOP_DELTA_LOGIN');

                $tranKey = base64_encode(sha1($nonce . $seed . $secretKey, true));

                $request = [
                    "auth" => [
                        "login" => $login,
                        "seed" => $seed,
                        "nonce" => $nonceBase64,
                        "tranKey" => $tranKey
                    ],
                    "internalReference" => $internal,
                ];
                $data = json_encode($request);
                //dd($data);
                //SOLICITAMOS LA CREACION DE SESSION PARA EL PAGO PLACETOPAY

                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => "https://test.placetopay.ec/redirection/api/reverse",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => $data,
                    CURLOPT_HTTPHEADER => array(
                        "Cache-Control: no-cache",
                        "Content-Type: application/json",
                        "Postman-Token: 96983e26-ba1b-4dfa-bbe9-54f877428d08"
                    ),
                    CURLOPT_USERAGENT => \request()->header('User-Agent'),
                ));
                $response = curl_exec($curl);
                $err = curl_error($curl);

                curl_close($curl);

            }else{
                if($resArray['status']['status'] == 'PENDING'){
                    $suscripcion = new Models\suscripciones();
                    $suscripcion->requestId = $requestId;
                    $suscripcion->type = $datreq['subscription']['type'];
                    $suscripcion->status = $resArray['status']['status'];
                    $suscripcion->message = $resArray['status']['message'];
                    $suscripcion->date = date("Y-m-d H:i:s");
                    $suscripcion->token = $datreq['subscription']['instrument'][0]['value'];
                    $suscripcion->document = $datreq['request']['payer']['document'];
                    $suscripcion->documentType = $datreq['request']['payer']['documentType'];
                    $suscripcion->name = $datreq['request']['payer']['name'];
                    $suscripcion->surname = $datreq['request']['payer']['surname'];
                    $suscripcion->email = $datreq['request']['payer']['email'];
                    $suscripcion->mobile = $datreq['request']['payer']['mobile'];
                    $suscripcion->fam_secuencia = $datreq['request']['fields'][0]['value'];
                    $suscripcion->fam_nombre = $famili[0]['fam_user'];
                    $suscripcion->save();
                    $mensaje = "Suscripción pendiente";
                }else {
                    $mensaje = "Suscripción fallida";
                }
            }
            //return response()->view('estadoPago', ['message'=>$mensaje, 200]);
            return view('estadoPago', [
                'message'=>$mensaje,
            ]);
            //return view('estadoPago', ['message' => $mensaje]);
        }
    }

    public function getRequestPtoP($requestId){
        $secretKey = env('PTOP_TORREMAR_SECRETKEY');
        $login = env('PTOP_TORREMAR_LOGIN');
        $seed = date('c');
        if (function_exists('random_bytes')) {
            $nonce = bin2hex(random_bytes(16));
        } elseif (function_exists('openssl_random_pseudo_bytes')) {
            $nonce = bin2hex(openssl_random_pseudo_bytes(16));
        } else {
            $nonce = mt_rand();
        }
        $nonceBase64 = base64_encode($nonce);
        $tranKey = base64_encode(sha1($nonce . $seed . $secretKey, true));

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => env('PLACETOPAY_URL')."api/session/".$requestId,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/json",
                "Postman-Token: 96983e26-ba1b-4dfa-bbe9-54f877428d08"
            ),
        ));
        $request = [
            "auth"=>[
                "login"=> $login,
                "seed"=> $seed,
                "nonce"=> $nonceBase64,
                "tranKey"=> $tranKey
            ]
        ];

        $data = json_encode($request);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return $err;
        } else {
            return $response;
        }
    }

}
