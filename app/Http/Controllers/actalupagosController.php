<?php

namespace App\Http\Controllers;

use Faker\Provider\DateTime;
use Illuminate\Http\Request;
use App\Models;
use App;



class actalupagosController extends Controller
{

    public function pagMatricula(Request $request)
    {

        if($_POST['alu_colegio']=='D'){
            $totPorPagar = $_POST['totPagarDelta'];
            $alumnas = $_POST['alumnasdelta'];
            $secretKey = env('PTOP_DELTA_SECRETKEY');
            $login = env('PTOP_DELTA_LOGIN');
        }
        if($_POST['alu_colegio']=='T'){
            $totPorPagar = $_POST['totPagarTorremar'];
            $alumnas = $_POST['alumnastorremar'];
            $secretKey = env('PTOP_TORREMAR_SECRETKEY');
            $login = env('PTOP_TORREMAR_LOGIN');
        }

        $fam_secuencia = $_POST['fam_secuencia'];


        //CREAMOS DATOS PARA LA AUTENTIFICACION EN PLACE TO PAY

        $seed = date('c');
        if (function_exists('random_bytes')) {
            $nonce = bin2hex(random_bytes(16));
        } elseif (function_exists('openssl_random_pseudo_bytes')) {
            $nonce = bin2hex(openssl_random_pseudo_bytes(16));
        } else {
            $nonce = mt_rand();
        }

        $nonceBase64 = base64_encode($nonce);

        $tranKey = base64_encode(sha1($nonce . $seed . $secretKey, true));

        //VALIDAMOS DATOS DEL COMPRADOR
        //$alumna = Models\acmalumna::where('alu_codalum', $alumnas[0])->get();
        $datComprador = Models\acmfamili::rightJoin('acmalumna', 'acmfamili.fam_secuencia', '=', 'acmalumna.fam_secuencia')
            ->where('acmalumna.alu_codalum', '=', $alumnas[0])
            ->get();

        if($datComprador[0]['alu_repres'] == 'M') {
            if ($datComprador[0]['fam_tipo_docmdr'] == 'C') {
                $docType = "CI";
            } else {
                if ($alumna[0]['fam_tip_docmdr'] == 'R') {
                    $docType = "NIT";
                } else {
                    $docType = "CI";
                }
            }
            $doc = $datComprador[0]['fam_ceduma'];
            $nombres = $datComprador[0]['fam_nommadr'];
            $apellidos = $datComprador[0]['fam_apematr'];
            $email = $datComprador[0]['fam_emailma'];
            $street = $datComprador[0]['fam_dirmadr'];
            $city = "";
            $state = "";
            $postalCode = "";
            $country = "EC";
            $phone = substr($datComprador[0]['fam_telmadr'],0,30);
            $mobile = substr($datComprador[0]['fam_telmovma'],0,30);

        }else{
            if ($datComprador[0]['fam_tipo_docpdr'] == 'C') {
                $docType = "CI";
            } else {
                if ($datComprador[0]['fam_tip_docpdr'] == 'R') {
                    $docType = "RUC";
                } else {
                    $docType = "CI";
                }
            }
            $doc = $datComprador[0]['fam_cedupa'];
            $nombres = $datComprador[0]['fam_nompadr'];
            $apellidos = $datComprador[0]['fam_apepadr'];
            $email = $datComprador[0]['fam_email00'];
            $street = $datComprador[0]['fam_dirpadr'];
            $city = "";
            $state = "";
            $postalCode = "";
            $country = "EC";
            $phone = substr($datComprador[0]['fam_telpadr'],0,30);
            $mobile = substr($datComprador[0]['fam_telmovp'],0,30);

        }

        $comprador = [
            "documentType" => $docType,
            "document" => $doc,
            "name" => $nombres,
            "surname" => $apellidos,
            "company" => null,
            "email" => 'jaime.mejia@ppm.com.ec',
            "address" => [
                "street" => $street,
                "city" => $city,
                "state" => $state,
                "postalCode" => $postalCode,
                "country" => $country,
                "phone" => $phone
            ],
            "mobile" => $mobile
        ];

        $pagador = null;

        if(isset($_POST['recurrente'])) {
            $reference = 'PR_' . time();
            $tipPago = 'PR';
            $recurring = [
                "periodicity"=> $_POST['periodicity'],
                "interval"=> $_POST['interval'],
                "nextPayment"=> $_POST['nextPayment'],
                "dueDate"=> $_POST['dueDate'],
                "notificationUrl"=> $_POST['notificationUrl'],
            ];
        }else{
            $reference = 'PN_' . time();
            $tipPago = 'PN';
            $recurring = null;
        }

        //SOLICITAMOS LA CREACION DE SESSION PARA EL PAGO PLACETOPAY
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => env('PLACETOPAY_URL')."api/session/",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/json",
                "Postman-Token: 96983e26-ba1b-4dfa-bbe9-54f877428d08"
            ),
        ));


            $request = [
                "auth" => [
                    "login" => $login,
                    "seed" => $seed,
                    "nonce" => $nonceBase64,
                    "tranKey" => $tranKey
                ],
                "buyer"=> $comprador,
                "locale" => 'es_EC',
                "payment" => [
                    "reference" => $reference, //ID generado por el comercio
                    "description" => "Pago Pensión COPECE - Delta",
                    "amount" => [
                        "currency" => "USD",
                        "total" => $totPorPagar,
                        "details" => [
                            [
                                "kind" => "subtotal",
                                "amount" => $totPorPagar

                            ]
                        ]
                    ],
                    "allowPartial" => false,
                ],
                "expiration" => date('c', strtotime('+15 minutes')), // tiempo para pago antes de caducar sesión
                "returnUrl" => url('/pagos/okplacetopay'),
                "ipAddress" => "200.7.213.242",
                "userAgent" => \request()->header('User-Agent'),
            ];

        $data = json_encode($request);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return $err;
        } else {
            $resArray = json_decode($response,true);
            if($resArray['status']['status'] == 'OK'){
                //creamos el registro del pago en la bd cliente
                $pago = new Models\actpagos();
                $pago->requestId = $resArray['requestId'];
                $pago->date = date("Y-m-d H:i:s");
                $pago->status = 'SOLICITADO';
                $pago->message = $resArray['status']['message'];;
                $pago->reference = $reference;
                $pago->signature = '';
                $pago->url = $resArray['processUrl'];
                $pago->monto = round($totPorPagar,2);
                $pago->tipo_transaccion = $tipPago;
                $pago->save();

                foreach ($alumnas as $alu_codalum){
                    $pagalu = new Models\actalupagos();
                    $pagalu->requestId = $resArray['requestId'];
                    $pagalu->alu_codalum = $alu_codalum;
                    $pagalu->status = 'P';
                    $pagalu->save();
                    //Models\acmalumna::where('alu_codalum',$alu_codalum)->update(['alu_estadomatriculado'=>'P']);
                }

            }
            return $resArray;
        }


        //return $response;
    }

    public function okplacetopay(Request $request)
    {
        $data = json_decode(file_get_contents('php://input'), true);
        $requestId = $data['requestId'];
        $datreq = $data;

        if($data['status']['status'] = 'APPROVED') {
            $alupagos = Models\actalupagos::leftJoin('actpagos', 'actpagos.requestId', '=', 'actalupagos.requestId')
                ->where('actpagos.requestId', '=', $requestId)
                ->update(['actalupagos.status' => 'A', 'actpagos.date' => date("Y-m-d H:i:s")]);

            $alumnas = Models\actalupagos::join('acmalumna', 'acmalumna.alu_codalum', '=', 'actalupagos.alu_codalum')
                ->where('actalupagos.requestId', '=', $requestId)
                ->update(['acmalumna.alu_estadomatriculado' => 'S', 'acmalumna.alu_fechaactualiza' => date("Y-m-d H:i:s")]);

            Models\actpagos::where('requestId', $requestId)->update(['status' => 'APPROVED', 'message' => 'La petición ha sido procesada correctamente']);
        }else{
            $alupagos = Models\actalupagos::leftJoin('actpagos', 'actpagos.requestId', '=', 'actalupagos.requestId')
                ->where('actpagos.requestId', '=', $requestId)
                ->update(['actalupagos.status' => 'P', 'actpagos.date' => date("Y-m-d H:i:s")]);

            $alumnas = Models\actalupagos::join('acmalumna', 'acmalumna.alu_codalum', '=', 'actalupagos.alu_codalum')
                ->where('actalupagos.requestId', '=', $requestId)
                ->update(['acmalumna.alu_estadomatriculado' => 'N', 'acmalumna.alu_fechaactualiza' => date("Y-m-d H:i:s")]);

            Models\actpagos::where('requestId', $requestId)->update(['status' => 'SOLICITADA', 'message' => 'La petición ha sido solicitada']);
        }

        if(!isset($data['subscription'])){

            $datreq = \GuzzleHttp\json_decode($this->getRequestPtoP($requestId),true);
            if(isset($datreq['subscription'])) {


            }
        }
        return $datreq;
    }

    public function cancelplacetopay()
    {
        $requestId = $_POST['requestId'];
        $message = $_POST['status']['message'];

        $alupagos = Models\actalupagos::leftJoin('actpagos', 'actpagos.requestId', '=', 'actalupagos.requestId')
            ->where('actpagos.requestId', '=', $requestId)
            ->update(['actalupagos.status'=>'R']);

        Models\actpagos::where('requestId',$requestId)
            ->update(['status'=>'REJECTED',
                    'message'=>$message,
                    'date'=> date("Y-m-d H:i:s"),
                    'detalle' => $message]);

        return $requestId;
    }

    public function comprobante(Request $request, $message)
    {
        if ($request->session()->has('user')) {
            $fam_secuencia = $request->session()->get('user');
            $famili = Models\acmfamili::where('fam_secuencia', $fam_secuencia)->get();
            $ok = $message;
            return view('comprobante', [
                'message' => $ok,
                'famili' => $famili,
            ]);
        }

        return view('login');

    }

    public function error(Request $request, $message){
        //var_dump($message);
        if ($request->session()->has('user')) {
            $fam_secuencia = $request->session()->get('user');
            $famili = Models\acmfamili::where('fam_secuencia', $fam_secuencia)->get();

            $error = $message;
            return view('error', [
                'famili' => $famili,
                'message' => $error,
            ]);
        }
        return view('login');
    }
    public function estadoPago(Request $request, $message){
        if ($request->session()->has('user')) {
            $fam_secuencia = $request->session()->get('user');
            $famili = Models\acmfamili::where('fam_secuencia', $fam_secuencia)->get();

            $estado = $message;
            return view('estadoPago',[
                'message' => $estado,
                'famili' => $famili,
            ]);
        }
        return view('login');
    }

    public function getRequestPtoP($requestId){
        $secretKey = env('PTOP_TORREMAR_SECRETKEY');
        $login = env('PTOP_TORREMAR_LOGIN');
        $seed = date('c');
        if (function_exists('random_bytes')) {
            $nonce = bin2hex(random_bytes(16));
        } elseif (function_exists('openssl_random_pseudo_bytes')) {
            $nonce = bin2hex(openssl_random_pseudo_bytes(16));
        } else {
            $nonce = mt_rand();
        }
        $nonceBase64 = base64_encode($nonce);
        $tranKey = base64_encode(sha1($nonce . $seed . $secretKey, true));

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => env('PLACETOPAY_URL')."api/session/".$requestId,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/json",
                "Postman-Token: 96983e26-ba1b-4dfa-bbe9-54f877428d08"
            ),
        ));
        $request = [
            "auth"=>[
                "login"=> $login,
                "seed"=> $seed,
                "nonce"=> $nonceBase64,
                "tranKey"=> $tranKey
            ]
        ];

        $data = json_encode($request);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return $err;
        } else {
            return $response;
        }
    }

}
