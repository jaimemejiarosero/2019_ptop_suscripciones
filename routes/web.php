<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Mail;
use App\Mail\envUsuarios;
use App\Models;
//Route::get('/', 'Controller@home');
Route::get('/pruebas', 'Controller@pruebas');
Route::post('/pruebas/pagos','Controller@pagPruebas');
Route::post('/pruebas/consultar','Controller@conPruebas');
Route::post('/pruebas/return/placetopay','Controller@returnplacetopay');
Route::post('/pruebas/cancel/placetopay','Controller@cancelplacetopay');



Route::get('/', 'acmfamiliController@login');

Route::get('/login', 'acmfamiliController@login');

Route::get('/enviar_clave', 'acmfamiliController@enviar_clave');

Route::get('/logout', 'acmfamiliController@logout');

Route::post('/login/valenvio','acmfamiliController@valEnvio');

Route::post('/login/valusuario','acmfamiliController@valUsuario');

Route::get('/pagos/index/{fam_secuencia}/{login}', 'acmfamiliController@index');

Route::get('/pagos/terminosycondiciones', 'acmfamiliController@terminos');

Route::get('/perfil', 'acmfamiliController@perfil');

Route::post('/perfil/guardar', 'acmfamiliController@perGuardar');

Route::get('/pagos/historial', 'acmfamiliController@historial');

Route::get('/pagos/faq', 'acmfamiliController@faq');

Route::post('/pagos/pagMatricula','actalupagosController@pagMatricula');

Route::post('/pagos/cancel/cancelplacetopay','actalupagosController@cancelplacetopay');

Route::post('/pagos/okplacetopay','actalupagosController@okplacetopay');

Route::get('/pagos/ok/comprobante/{message}','actalupagosController@comprobante');

Route::get('/pagos/error/{message}','actalupagosController@error');

Route::get('/pagos/estado/{message}','actalupagosController@estadoPago');

Route::get('/suscripciones/index','suscripcionController@index');

Route::get('/suscripciones','suscripcionController@suscripcion');

Route::post('/suscripciones/cobros','suscripcionController@susCobros');

Route::get('/suscripciones/cobros/ver/{fam_secuencia}','suscripcionController@verCobros');

Route::get('/suscripciones/cobro/dolar/{requestId}','suscripcionController@susCobroDolar');

Route::get('/pagos/consulta/{requestId}','actalupagosController@getRequestPtoP');

//CROND
Route::get('/pagos/pendientes','actpagosController@pendientes');






Route::get('email', function (){
    $famili = Models\acmfamili::all();
    foreach ($famili as $user) {

        try{
            Mail::to($user->fam_email00, $user->fam_apepadr . ' ' . $user->fam_nompadr)
                ->send(new envUsuarios($user));

        } catch(Exception $e){
            echo 'ERROR - Email no valido: '. $user->fam_email00.' del usuario:'. $user->fam_apepadr . ' ' . $user->fam_nompadr;
            echo "<br />";
        }
        echo 'OK - Email valido: '. $user->fam_email00.' del usuario:'. $user->fam_apepadr . ' ' . $user->fam_nompadr;
        echo "<br />";
        try{
            Mail::to($user->fam_emailma, $user->fam_apemadr . ' ' . $user->fam_nommadr)
                ->send(new envUsuarios($user));

        } catch(Exception $e){
            echo 'ERROR - Email no valido: '. $user->fam_emailma.' del usuario:'. $user->fam_apemadr . ' ' . $user->fam_nommadr;
            echo "<br />";
        }
        echo 'OK - Email valido: '. $user->fam_emailma.' del usuario:'. $user->fam_apemadr . ' ' . $user->fam_nommadr;
        echo "<br />";

    }
});
