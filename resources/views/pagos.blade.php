@extends('templates.master')

@section('title', 'Pagos')
@section('page_title')
    <<h1><i class="fas fa-tv"></i> Inicio</h1>
@endsection
@section('page_usuario')
    <<h1><i class="far fa-user-circle"></i> {{$famili[0]['fam_user']}}</h1>
@endsection


@section('content')
       <table id="principal" width="100%" style="background-color: white;">
           <tr>
               <td width="100%" >
                   <table width="100%">
                       <tr>
                           @section('menu')
                                       <tr>
                                           <td style="vertical-align: center">
                                               <a href="{{action('acmfamiliController@login')}}" style="text-decoration: none">
                                                   <div style="width:100%; height:100%">
                                                       <table><tr><td>
                                                                   <img src="{{asset("imgCopece/ico_ini.png")}}" style="margin-left:10px"/>
                                                               </td><td><h4>Pagos</h4></td></tr></table>
                                                       <br>
                                                   </div>
                                               </a>
                                           </td>
                                       </tr>
                                       @if($famili[0]['fam_user'] == 'ADMINISTRADOR')
                                           <tr>
                                               <td style="vertical-align: center">
                                                   <a href="{{action('suscripcionController@index')}}" style="text-decoration: none">
                                                       <div style="width:100%; height:100%">
                                                           <table><tr><td>
                                                                       <img src="{{asset("imgCopece/ico_per.png")}}" style="margin-left:10px"/>
                                                                   </td>
                                                                   <td><h4>Suscripciones</h4></td></tr></table>
                                                           <br>
                                                       </div>
                                                   </a>
                                               </td>
                                           </tr>
                                       @else
                                           <tr>
                                               <td style="vertical-align: center">
                                                   <a href="{{action('acmfamiliController@historial')}}" style="text-decoration: none">
                                                       <div style="width:100%; height:100%">
                                                           <table><tr><td>
                                                                       <img src="{{asset("imgCopece/ico_his.png")}}" style="margin-left:10px"/>
                                                                   </td><td><h4>Historial Pagos</h4></td></tr></table>
                                                           <br>
                                                       </div>
                                                   </a>
                                               </td>
                                           </tr>
                                           <tr>
                                               <td style="vertical-align: center">
                                                   <a href="{{action('acmfamiliController@faq')}}" style="text-decoration: none">
                                                       <div style="width:100%; height:100%">
                                                           <table><tr><td>
                                                                       <img src="{{asset("imgCopece/ico_his.png")}}" style="margin-left:10px"/>
                                                                   </td>
                                                                   <td><h4>Preguntas Frecuentes</h4></td></tr></table>
                                                           <br>
                                                       </div>
                                                   </a>
                                               </td>
                                           </tr>
                                       @endif



                                       <tr>
                                           <td style="vertical-align: center">
                                               <a href="{{action('acmfamiliController@logout')}}" style="text-decoration: none">
                                                   <div style="width:100%; height:100%">
                                                       <table><tr><td>
                                                                   <img src="{{asset("imgCopece/ico_sal.png")}}" style="margin-left:10px"/>
                                                               </td><td><h4>Salir</h4></td></tr></table>
                                                       <br>
                                                   </div>
                                               </a>
                                           </td>
                                       </tr>
                           @endsection
                           <td width="85%" style="vertical-align: top;background-color: #EDEDED">
                               <table width="100%" style="height:95%; ">

                                   <tr>
                                       <td colspan="3" width="100%" valign="top" style="background-color: #EDEDED">
                                           <br>
                                           <div class="scrollable" style="overflow-y:auto;">
                                               <table style="background-color: #EDEDED; border-collapse: collapse;  border-radius: 8px;  overflow: hidden; width: 100%; height:100%">
                                                   <tr>
                                                       <td align="left" width="20px">
                                                           <img src="{{asset("imgCopece/b_ver.png")}}" style="margin-left:30px;width:20px"/>
                                                       </td>
                                                       <td align="left">
                                                           <h4>Información del Padre</h4>
                                                       </td>
                                                   </tr>
                                                   <tr>
                                                       <td colspan="2">
                                                           <div class="table-responsive" align="left" style="width:100%" >
                                                               <table class="table table-bordered table-sm" style="font-size: small; width:90%;margin-left: 50px;" >
                                                                   <thead class="table-striped" style="background-color: #9E997B;color: white">
                                                                   <tr>
                                                                       <th scope="col">Nombre</th>
                                                                       <th scope="col">Cédula</th>
                                                                       <th scope="col">Nacionalidad</th>
                                                                       <th scope="col">Dirección</th>
                                                                       <th scope="col">Teléfono</th>
                                                                       <th scope="col">Email</th>
                                                                       <th scope="col">Opciones</th>
                                                                   </tr>
                                                                   </thead>
                                                                   <tbody>
                                                                   @foreach ($famili as $fam)
                                                                       <tr style="background-color: white">
                                                                           <td><h5>{{$fam['fam_apepadr']}} {{$fam['fam_nompadr']}}</h5></td>
                                                                           <td><h5>{{$fam['fam_cedupa']}}</h5> </td>
                                                                           <td><h5>{{$fam['fam_nacpadr']}}</h5> </td>
                                                                           <td><h5>{{$fam['fam_dirpadr']}}</h5> </td>
                                                                           <td><h5>{{$fam['fam_telpadr']}}</h5> </td>
                                                                           <td><h5>{{$fam['fam_email00']}}</h5> </td>
                                                                           <td align="center" valign="center"><h4 style="font-size: 15px"><a href="{{action('acmfamiliController@perfil')}}" >Editar</a></h4></td>
                                                                       </tr>
                                                                   @endforeach

                                                                   </tbody>
                                                               </table>
                                                           </div>
                                                       </td>
                                                   </tr>
                                                   
                                                   <tr>
                                                       <td align="left" width="20px">
                                                           <img src="{{asset("imgCopece/b_ver.png")}}" style="margin-left:30px;width:20px"/>
                                                       </td>
                                                       <td align="left">
                                                           <h4>Información de la Madre</h4>
                                                       </td>
                                                   </tr>
                                                   <tr>
                                                       <td colspan="2">
                                                           <div class="table-responsive" align="left" width="100%" >
                                                               <table class="table table-bordered table-sm" style="font-size: small; width:90%;margin-left: 50px;" >
                                                                   <thead class="table-striped" style="background-color: #9E997B;color: white">
                                                                   <tr>
                                                                       <th scope="col">Nombre</th>
                                                                       <th scope="col">Cédula</th>
                                                                       <th scope="col">Nacionalidad</th>
                                                                       <th scope="col">Dirección</th>
                                                                       <th scope="col">Teléfono</th>
                                                                       <th scope="col">Email</th>
                                                                       <th scope="col">Opciones</th>
                                                                   </tr>
                                                                   </thead>
                                                                   <tbody>
                                                                   @foreach ($famili as $fam)
                                                                       <tr style="background-color: white">
                                                                           <td><h5>{{$fam['fam_apemadr']}} {{$fam['fam_nommadr']}}</td>
                                                                           <td><h5>{{$fam['fam_ceduma']}}</h5> </td>
                                                                           <td><h5>{{$fam['fam_nacmadr']}}</h5> </td>
                                                                           <td><h5>{{$fam['fam_dirmadr']}}</h5> </td>
                                                                           <td><h5>{{$fam['fam_telmadr']}}</h5> </td>
                                                                           <td><h5>{{$fam['fam_emailma']}}</h5> </td>
                                                                           <td align="center" valign="center"><h4 style="font-size: 15px"><a href="{{action('acmfamiliController@perfil')}}" >Editar</a></h4></td>
                                                                       </tr>
                                                                   @endforeach

                                                                   </tbody>
                                                               </table>
                                                           </div>
                                                       </td>
                                                   </tr>
                                                   @if($famili[0]['fam_aperep1'] != '')

                                                   <tr>
                                                       <td align="left" width="20px">
                                                           <img src="{{asset("imgCopece/b_ver.png")}}" style="margin-left:30px;width:20px"/>
                                                       </td>
                                                       <td align="left">
                                                           <h4>Información Representante Legál</h4>
                                                       </td>
                                                   </tr>
                                                   <tr>
                                                       <td colspan="2">
                                                               <div class="table-responsive" align="left" width="100%" >
                                                                   <table class="table table-bordered table-sm" style="font-size: small; width:90%;margin-left: 50px;" >
                                                                       <thead class="table-striped" style="background-color: #9E997B;color: white">
                                                                       <tr>
                                                                           <th scope="col">Nombre</th>
                                                                           <th scope="col">Cédula</th>
                                                                           <th scope="col">Dirección</th>
                                                                           <th scope="col">Teléfono</th>
                                                                           <th scope="col">Email</th>
                                                                       </tr>
                                                                       </thead>
                                                                       <tbody>
                                                                       @foreach ($famili as $fam)
                                                                           <tr style="background-color: white">
                                                                               <td><h5>{{$fam['fam_replegal']}}</h5> </td>
                                                                               <td><h5>{{$fam['fam_cedruc_replegal']}}</h5> </td>
                                                                               <td><h5>{{$fam['fam_dirofip_csv']}}</h5> </td>
                                                                               <td><h5>{{$fam['fam_telmovre']}}</h5> </td>
                                                                               <td><h5>{{$fam['fam_emailre']}}</h5> </td>
                                                                           </tr>
                                                                       @endforeach

                                                                       </tbody>
                                                                   </table>
                                                               </div>
                                                       </td>
                                                   </tr>
                                                   @endif


                                               <!--DELTA-->
                                               @if(count($alumnasDelta) > 0)
                                                   <tr>
                                                       <td align="left" width="20px">
                                                           <!--<img src="{{asset("imgCopece/b_ver.png")}}" style="margin-left:30px;width:20px"/>-->
                                                           <img src="{{asset("imgCopece/logo_delta.png")}}" style="margin-left:50px;width:40px"/>
                                                       </td>
                                                       <td align="left">
                                                           @if($urldelta != '')
                                                               <h4>Solicitudes de Pago Pendientes Delta </h4>
                                                           @else
                                                               <h4>Información de los Estudiantes Delta</h4>
                                                           @endif
                                                       </td>
                                                   </tr>
                                                   <tr>
                                                       <td colspan="2">
                                                          <!-- ////////////////////////////////ESTUDIANTES///////////////////////////////////////////////////-->
                                                           @if($urldelta != '')
                                                               <div class="table-responsive" align="left" width="100%" >
                                                                   {{csrf_field()}}

                                                                   <table class="table table-bordered table-sm table-striped" style="font-size: small; width:90%;margin-left: 50px" >
                                                                       <thead class="table-striped" style="background-color: #9E997B;color: white">
                                                                       <tr>
                                                                           <th scope="col">Id</th>
                                                                           <th scope="col">Fecha</th>
                                                                           <th scope="col">Referencia</th>
                                                                           <th scope="col">Estado</th>
                                                                           <th></th>
                                                                       </tr>
                                                                       </thead>
                                                                       <tbody>
                                                                       <tr>
                                                                           <th scope="row"><h5>{{$aluPagosDelta[0]['requestId']}}</h5></th>
                                                                           <td><h5>{{$aluPagosDelta[0]['date']}}</h5></td>
                                                                           <td><h5>{{$aluPagosDelta[0]['reference']}}</h5></td>
                                                                           <td><h5>{{$aluPagosDelta[0]['status']}}</h5></td>
                                                                           <td align="center">
                                                                               <h4 style="font-size: 15px">El pago esta siendo procesado,<br>
                                                                                   por favor espere unos minitos.</h4>
                                                                               <!--<button type="button" id="btprocesardelta" class="btn btn-success">Continuar Pago</button>-->
                                                                               <!-- <button type="button" id="btcancelar" class="btn btn-danger">Cancelar Pago</button>-->
                                                                           </td>
                                                                       </tr>
                                                                       </tbody>
                                                                   </table>
                                                               </div>

                                                           @else
                                                               <div class="table-responsive" align="left" width="100%" >
                                                                   <form id="forPagoDelta" method="post" action="{{ action('actalupagosController@pagMatricula') }}" accept-charset="UTF-8" onsubmit="return validateForm()">
                                                                       {{csrf_field()}}
                                                                       <table class="table table-bordered table-sm table-striped" style="font-size: small; width:90%; margin-left: 50px" >
                                                                           <thead class="table-striped" style="background-color: #9E997B;color: white">
                                                                           <tr>
                                                                               <th scope="col"><input class="checkbox-inline" style="font-size: large;" type="checkbox" id="select_all_delta"/></th>
                                                                               <th scope="col">Código</th>
                                                                               <th scope="col">Cédula</th>
                                                                               <th scope="col">Nombre</th>
                                                                               <th scope="col">Ciudad</th>
                                                                               <th scope="col">Dirección</th>
                                                                               <th scope="col">Teléfono</th>
                                                                               <th scope="col">Curso</th>
                                                                               <th scope="col">Estado</th>
                                                                               <th scope="col">Valor</th>
                                                                           </tr>
                                                                           </thead>
                                                                           <tbody>

                                                                           @foreach ($alumnasDelta as $alumnadelta)
                                                                               <tr style="background-color: white">
                                                                                   @if($alumnadelta['alu_estadomatriculado']!='S')
                                                                                       <th scope="row">
                                                                                           <input class="checkboxdelta" style="font-size: large;" type="checkbox" id="{{$alumnadelta['alu_codalum']}}" name="alumnasdelta[]" value="{{$alumnadelta['alu_codalum']}}"></th>
                                                                                   @else
                                                                                       <th scope="row"></th>
                                                                                   @endif
                                                                                   <th scope="row">{{$alumnadelta['alu_codalum']}}</th>
                                                                                   <td><h5>{{$alumnadelta['alu_numcedu']}}</h5></td>
                                                                                   <td><h5>{{$alumnadelta['alu_nomcomp']}}</h5></td>
                                                                                   <td><h5>{{$alumnadelta['alu_codciud']}}</h5></td>
                                                                                   <td><h5>{{$alumnadelta['alu_direcci']}}</h5></td>
                                                                                   <td><h5>{{$alumnadelta['alu_numtelf']}}</h5></td>
                                                                                   <td><h5>{{$alumnadelta['minidescrip']}}</h5></td>
                                                                                   <td>@if($alumnadelta['alu_estadomatriculado']=='S') <h6 style="color: #2ca02c; font-size: small">PAGADO</h6> @else <h6 style="color: red; font-size: small">PENDIENTE</h6> @endif</td>
                                                                                   <th scope="row">$25</th>

                                                                               </tr>
                                                                           @endforeach

                                                                           </tbody>
                                                                       </table>
                                                                       <input type="text" value="D" id="alu_colegio" name="alu_colegio" style="display:none;"/>
                                                                       <input type="text" value="{{$famili[0]['fam_secuencia']}}" id="fam_secuencia" name="fam_secuencia" style="display:none;"/>
                                                                       <input class="checkbox-inline" style="font-size: large;display:none" type="checkbox" id="titularDelta" name="titularDelta" checked value="on"/>
                                                                       <table class="tble table-bordered table-sm table-stripeda" style="font-size: small; width:90%; margin-left: 50px" >
                                                                           <tr>
                                                                               <td align="right" width="100%" colspan="2" valign="top" style="background-color: white">
                                                                                   <input readonly id="totPagarDelta"  name="totPagarDelta" value="0" style="display:none;text-align: right;width: 125px">
                                                                                   <input readonly id="requestId"  name="requestId" value="" style="display:none;text-align: right;width: 125px">
                                                                                   <div id="vtotPagarDelta"><h4 style="font-size: 15px;font-family: 'Helvetica Neue', Verdana, Geneva, Arial, Helvetica, sans-serif;">Total a Pagar: $0</h4></div>
                                                                               </td>
                                                                           </tr>


                                                                           <tr>
                                                                               <td colspan="2" align="center" width="60%" style="border: hidden;">
                                                                                   <div id="aceD">
                                                                                       <br>
                                                                                       <input class="checkbox-inline" style="font-size: large;margin-left: 0px" type="checkbox" id="aceptoDelta" name="aceptoDelta"  />   Acepto <a href="{{action('acmfamiliController@terminos')}}">Terminos y condiciones</a>
                                                                                       <br>
                                                                                       <br>
                                                                                   </div>
                                                                               </td>

                                                                           </tr>

                                                                           <tr>
                                                                               <td align="right" valign="top" style="border: hidden">
                                                                                   @if (count($suscripciones) > 0)
                                                                                       <button type="button" id="_2" class="btn btn-success" style="background-color: dodgerblue; width: 200px">Ya se encuentra Suscrito</button>
                                                                                   @else
                                                                                        <button type="button" id="btsuscripcion" class="btn btn-success" disabled style="background-color: #2ca02c; width: 200px">Suscribirse</button>
                                                                                   @endif

                                                                               </td>
                                                                               <td align="left" valign="top" width="50%" style="border: hidden">
                                                                                   <a id="hrefD" href="#" data-toggle="modal" data-target="#placetopay" style="text-decoration: none">
                                                                                       <button type="button" id="btpagardelta" class="btn btn-success" disabled style="background-color: #010A35; width: 200px;display:block; ">Realizar Pago</button><br>
                                                                                   </a>
                                                                               </td>
                                                                           </tr>
                                                                           <tr>
                                                                               <td colspan="2" align="center" valign="top" width="100%" style="border: hidden">
                                                                                   <img id="imgptpD" src="{{asset("imgCopece/no_logo.svg")}}" width="150px" style="margin-left: 0px">
                                                                                   <br><br>
                                                                               </td>
                                                                           </tr>
                                                                       </table>
                                                       <tr>
                                                           <td>
                                                               <br>
                                                               <!--<input type="checkbox" id="recurrente" name="recurrente" value="on"/>   Pago Recurrente?<br><br>-->
                                                               <div id="pagRecurrente" style="display:none">
                                                                   periodicidad <input id="periodicity" name="periodicity" value="M" /><br>
                                                                   intervalo <input id="interval" name="interval" value="1" /><br>
                                                                   proximo pago <input id="nextPayment" name="nextPayment" value="2018-04-11" /><br>
                                                                   numero de pagos <input id="maxPeriods" name="maxPeriods" value="12" /><br>
                                                                   fecha caducidad <input id="dueDate" name="dueDate" value="2019-04-11" /><br>
                                                                   Url de notidicación <input id="notificationUrl" name="notificationUrl" value="http://localhost:8000/pagos/ok/okplacetopay/" />

                                                               </div>
                                                           </td>
                                                       </tr>

                                                                   </form>
                                                               </div>
                                                           @endif
                                                       </td>
                                                   </tr>
                                                @endif
                                                    <!-- FIN DELTA-->

                               </table>
                                           </div>
                                       </td>
                                   </tr>
                               </table>
                           </td>
                       </tr>
                   </table>
               </td>
           </tr>
       </table>
@endsection
@section('js')
    @parent

    <script>
        var totpagardelta = 0;
        var totpagartorremar = 0;
        $(document).ready(function () {
//            $("#recurrente").change(function () {
  //              if(this.checked == false) {
    //                $("#pagRecurrente").hide();
      //          }else{
        //            $("#pagRecurrente").show();
          //      }
            //});

            //$("#btprocesardelta").click(function () {

             //   P.init('{{$urldelta}}');
            //    $(".close-frame").hide();
            //});

            $("#btpagardelta").click(function () {
                var totPagar = document.forms["forPagoDelta"]["totPagarDelta"].value;
                if (totPagar == 0) {
                    alert("Seleccione el alumno, para realizar el pago de las pensiones.");
                    return false;
                }

                var url = $("#forPagoDelta").attr('action');
                var data = $("#forPagoDelta").serialize();
                $.ajax({
                    headers: { 'X-CSRF-Token': $('input[name="_token"]').val() },
                    type: 'POST',
                    url: url,
                    data: data,
                    dataType: 'json',
                    success: function (data) {
                        if (data.status.status == 'OK'){
                            P.init(data.processUrl);
                        }else{
                            window.location = "{{URL::to('/pagos/estado/')}}" + "/" + data.status.message;
                        }
                        $(".close-frame").hide();
                    },
                    error: function (data) {
                        console.log(data);
                         //window.location = "{{URL::to('/pagos/estado/')}}" + "/Su sessión a caducado, por favor ingrese nuevamente.";
                    }
                });

            });

            if('{{$urldelta}}' == '') {
                var select_all_delta = document.getElementById("select_all_delta"); //select all checkbox
                var checkboxesdelta = document.getElementsByClassName("checkboxdelta"); //checkbox items
                if(checkboxesdelta.length == 0){
                    $("#select_all_delta").hide();
                    $("#aceD").show();
                    $("#imgptpD").hide();
                    $("#hrefD").hide();
                    $("#titularDelta").hide();
                }
                select_all_delta.addEventListener("change", function (e) {
                    if(select_all_delta.checked == true){
                        totpagardelta=0;
                    }
                    for (i = 0; i < checkboxesdelta.length; i++) {
                        checkboxesdelta[i].checked = select_all_delta.checked;

                        if (checkboxesdelta[i].checked == false) {
                            totpagardelta = totpagardelta - 25;
                        } else {
                            totpagardelta = totpagardelta + 25;
                        }
                        $("#vtotPagarDelta").html("<h4 style='font-size: 15px;font-family: Helvetica Neue, Verdana, Geneva, Arial, Helvetica, sans-serif;'>Total a Pagar: $" + totpagardelta+"</h4>");
                        $("#totPagarDelta").val(totpagardelta);
                    }
                });

                for (var i = 0; i < checkboxesdelta.length; i++) {
                    checkboxesdelta[i].addEventListener('change', function (e) { //".checkbox" change
                        if (this.checked == false) {
                            totpagardelta = totpagardelta - 25;
                        } else {
                            totpagardelta = totpagardelta + 25;
                        }
                        $("#vtotPagarDelta").html("<h4 style='font-size: 15px;font-family: Helvetica Neue, Verdana, Geneva, Arial, Helvetica, sans-serif;\'>Total a Pagar: $" + totpagardelta+"</h4>");
                        $("#totPagarDelta").val(totpagardelta);

                        if (document.querySelectorAll('.checkboxdelta:checked').length == checkboxesdelta.length) {
                            select_all_delta.checked = true;
                        }else{
                            select_all_delta.checked = false;
                        }
                    });
                }
            }

            //SUSCRIPCION

            $("#btsuscripcion").click(function () {
                if(!$("#aceptoDelta").prop('checked')){
                    alert("Debe Aceptar Terminos y Condiciones");
                    return false;
                }
                $.ajax({
                    type: 'GET',
                    url: '{{action('suscripcionController@suscripcion')}}',
                    success: function (data) {
                        console.log(data);
                        if (data.status.status == 'OK'){
                            P.init(data.processUrl);
                        }else{
                            window.location = "{{URL::to('/pagos/estado/')}}" + "/" + data.status.message;
                        }
                        $(".close-frame").hide();
                    },
                    error: function (data) {
                        console.log(data);
                        //window.location = "{{URL::to('/pagos/estado/')}}" + "/Su sessión a caducado, por favor ingrese nuevamente.";
                    }
                });

            });

            $("#btpagartorremar").click(function () {
                var totPagar = document.forms["forPagoTorremar"]["totPagarTorremar"].value;
                if (totPagar == 0) {
                    alert("Seleccione el alumno, para realizar el pago de las pensiones.");
                    return false;
                }

                var url = $("#forPagoTorremar").attr('action');
                var data = $("#forPagoTorremar").serialize();
                console.log(data);
                $.ajax({
                    headers: { 'X-CSRF-Token': $('input[name="_token"]').val() },
                    type: 'POST',
                    url: url,
                    data: data,
                    dataType: 'json',
                    success: function (data) {
                        console.log(data);
                        if (data.status.status == 'OK'){
                            P.init(data.processUrl);
                        }else{
                            window.location = "{{URL::to('/pagos/estado/')}}" + "/" + data.status.message;
                        }
                        $(".close-frame").hide();
                    },
                    error: function (data) {
                        console.log(data);
                        window.location = "{{URL::to('/pagos/estado/')}}" + "/Su sessión a caducado, por favor ingrese nuevamente.";
                    }
                });

            });

            if('{{$urltorremar}}' == '') {
                var select_all_torremar = document.getElementById("select_all_torremar"); //select all checkbox
                var checkboxes = document.getElementsByClassName("checkboxtorremar"); //checkbox items
                if(checkboxes.length == 0){
                    $("#select_all_torremar").hide();
                    $("#aceT").hide();
                    $("#imgptpT").hide();
                    $("#hrefT").hide();
                    $("#titularTorremar").hide();
                }

                for (var i = 0; i < checkboxes.length; i++) {
                    checkboxes[i].addEventListener('change', function (e) { //".checkbox" change
                        if (this.checked == false) {
                            totpagartorremar = totpagartorremar - 25;
                        } else {
                            totpagartorremar = totpagartorremar + 25;
                        }
                        $("#vtotPagarTorremar").html("<h4 style='font-size: 15px;font-family: Helvetica Neue, Verdana, Geneva, Arial, Helvetica, sans-serif;\'>Total a Pagar: $" + totpagartorremar+"</h4>");
                        $("#totPagarTorremar").val(totpagartorremar);

                        if (document.querySelectorAll('.checkboxtorremar:checked').length == checkboxestorremar.length) {
                            select_all_torremar.checked = true;
                        }else{
                            select_all_torremar.checked = false;
                        }
                    });
                }
            }

        });

        //CAPTURA RESPUESTA DADA POR LIGHTBOX PLACETOPAY PROCESO NORMAL
        P.on('response', function(data) {
            console.log(data);
            var datajson = JSON.stringify(data);
            if(data['status']['status'] == 'REJECTED'){
                $.ajax({
                    headers: { 'X-CSRF-Token': $('input[name="_token"]').val() },
                    type: 'POST',
                    url: '{{action('actalupagosController@cancelplacetopay')}}',
                    data: data,
                    dataType: 'json',
                    success: function (requestId) {
                        console.log(requestId);
                        window.location = "{{URL::to('/pagos/estado/')}}" + "/" + data['status']['message'];
                    },
                    error: function (data) {
                        console.log(data);
                        window.location = "{{URL::to('/pagos/error/')}}" + "/" + data.statusText;
                    }
                });
            }else{
                if(data['status']['status'] == 'APPROVED') {
                    $.ajax({
                        headers: {'X-CSRF-Token': $('input[name="_token"]').val()},
                        type: 'POST',
                        url: '{{action('actalupagosController@okplacetopay')}}',
                        data: datajson,
                        dataType: 'json',
                        success: function (data) {
                            console.log(data.subscription);
                            if(data.subscription) {
                                urr = "{{action('suscripcionController@susCobroDolar',['requestId'=>'remplazar'])}}";
                                window.location = urr.replace('remplazar',data['requestId']);
                            }else{
                                window.location = "{{URL::to('/pagos/estado/')}}" + "/" + data['status']['message'];
                            }
                        },
                        error: function ($message) {
                            console.log(data);
                            window.location = "{{URL::to('/pagos/error/')}}" + "/" + data.statusText;
                        }
                    });
                }else{
                    //window.location = "{{URL::to('/pagos/estado/')}}" + "/" + data['status']['message'];
                }
            }
        });

        $("#aceptoDelta").change(function(){
            if (this.checked == false) {
                $("#btpagardelta").attr('disabled','disabled');
                $("#btsuscripcion").attr('disabled','disabled');
            }else{
                $("#btpagardelta").removeAttr("disabled");
                $("#btsuscripcion").removeAttr("disabled");
            }
        });
        $("#aceptoTorremar").change(function(){
            if (this.checked == false) {
                $("#btpagartorremar").attr('disabled','disabled');
            }else{
                $("#btpagartorremar").removeAttr("disabled");
            }
        });
    </script>
@endsection
