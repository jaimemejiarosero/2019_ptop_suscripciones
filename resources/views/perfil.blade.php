@extends('templates.master')

@section('title', 'Pagos')
@section('page_title')
    <h1><i class="far fa-user"></i> Perfil</h1>
@endsection
@section('page_usuario')
    <<h1><i class="far fa-user-circle"></i> {{$famili[0]['fam_user']}}</h1>
@endsection

@section('content')
    <table id="principal" width="100%" style="background-color: white" valign="top">
        <tr>
            <td width="100%" valign="top">
                <table width="100%">
                    <tr>
                    @section('menu')
                        <tr>
                            <td style="vertical-align: center">
                                <a href="{{action('acmfamiliController@login')}}" style="text-decoration: none">
                                    <div style="width:100%; height:100%">
                                        <table><tr><td>
                                                    <img src="{{asset("imgCopece/ico_ini.png")}}" style="margin-left:10px"/>
                                                </td><td><h4>Inicio</h4></td></tr></table>
                                        <br>
                                    </div>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: center">
                                <a href="{{action('acmfamiliController@perfil')}}" style="text-decoration: none">
                                    <div style="width:100%; height:100%">
                                        <table><tr><td>
                                                    <img src="{{asset("imgCopece/ico_per.png")}}" style="margin-left:10px"/>
                                                </td><td><h4>Perfil</h4></td></tr></table>
                                        <br>
                                    </div>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: center">
                                <a href="{{action('acmfamiliController@historial')}}" style="text-decoration: none">
                                    <div style="width:100%; height:100%">
                                        <table><tr><td>
                                                    <img src="{{asset("imgCopece/ico_his.png")}}" style="margin-left:10px"/>
                                                </td><td><h4>Historial Pagos</h4></td></tr></table>
                                        <br>
                                    </div>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: center">
                                <a href="{{action('acmfamiliController@faq')}}" style="text-decoration: none">
                                    <div style="width:100%; height:100%">
                                        <table><tr><td>
                                                    <img src="{{asset("imgCopece/ico_his.png")}}" style="margin-left:10px"/>
                                                </td>
                                                <td><h4>Preguntas Frecuentes</h4></td></tr></table>
                                        <br>
                                    </div>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: center">
                                <a href="{{action('acmfamiliController@logout')}}" style="text-decoration: none">
                                    <div style="width:100%; height:100%">
                                        <table><tr><td>
                                                    <img src="{{asset("imgCopece/ico_sal.png")}}" style="margin-left:10px"/>
                                                </td><td><h4>Salir</h4></td></tr></table>
                                        <br>
                                    </div>
                                </a>
                            </td>
                        </tr>
                    @endsection
                        <td width="85%" align="center" style="vertical-align: top;background-color: #EDEDED">
                            <table width="98%">
                                <tr>
                                    <td colspan="3" align="center" width="100%" valign="top" style="background-color: #EDEDED">
                                            <form id="forPerfil" method="post" action="{{ action('acmfamiliController@perGuardar') }}" accept-charset="UTF-8" onsubmit="return validateForm()">
                                                {{csrf_field()}}
                                                <table style="background-color: white; border-collapse: collapse;  border-radius: 8px;  overflow: hidden; width: 100%; height:100%" align="center">
                                                    <tr>
                                                        <td align="center" colspan="2" style="margin-left:30px;">
                                                            <h4>Información del Padre</h4>
                                                            <br>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="" colspan="2" style="margin-left:0px;">
                                                            <h4>Datos Generales</h4>
                                                            <img src="{{asset('imgCopece/line.png')}}"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label style="margin-left:30px;" for="nombres">Nombres</label>
                                                        </td>
                                                        <td>
                                                            <label for="apellidos">Apellidos</label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input type="text"  maxlength="60" id="nombrespa" name="nombrespa" value="{{$famili[0]['fam_nompadr']}}" class="form-control" style="width:80%;height:25px;margin-left:30px;" />
                                                        </td>
                                                        <td>
                                                            <input type="text" maxlength="60" class="form-control"  value="{{$famili[0]['fam_apepadr']}}" id="apellidospa" name="apellidospa" style="width:80%;height:25px" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label style="margin-left:30px;" for="nombres">Número de Cédula</label>
                                                        </td>
                                                        <td>
                                                            <label for="apellidos">Dirección domiciliaria</label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input type="text" maxlength="15" value="{{$famili[0]['fam_cedupa']}}"id="cedulapa" name="cedulapa" class="form-control" style="width:80%;height:25px;margin-left: 30px"/>
                                                        </td>
                                                        <td>
                                                            <input type="text" maxlength="255" value="{{$famili[0]['fam_dirpadr']}}" id="direccionpa" name="direccionpa" class="form-control" style="width:80%;height:25px"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label style="margin-left:30px;" for="nombres">Teléfonos</label>
                                                        </td>
                                                        <td>
                                                            <label for="apellidos">Correo Electrónico</label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input type="text" maxlength="50" value="{{$famili[0]['fam_telpadr']}}" id="telefonopa" name="telefonopa" class="form-control" style="width:80%;height:25px; margin-left: 30px"/>
                                                        </td>
                                                        <td>
                                                            <input type="email" value="{{$famili[0]['fam_email00']}}" id="emailpa" name="emailpa" class="form-control" style="width:80%;height:25px"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="" colspan="2" style="margin-left:0px;">
                                                            <br>
                                                            <h4>Datos Profesionales</h4>
                                                            <img src="{{asset('imgCopece/line.png')}}"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label style="margin-left:30px;" for="nombres">Lugar de Trabajo</label>
                                                        </td>
                                                        <td>
                                                            <label for="apellidos">Cargo</label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input type="text" maxlength="50" value="{{$famili[0]['fam_trapadr']}}" id="trabajopa" name="trabajopa" class="form-control" style="width:80%;height:25px; margin-left: 30px"/>
                                                        </td>
                                                        <td>
                                                            <input type="text" value="{{$famili[0]['fam_carpadr']}}" id="cargopa" name="cargopa" class="form-control" style="width:80%;height:25px"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label style="margin-left:30px;" for="nombres">Titulo</label>
                                                        </td>
                                                        <td>
                                                            <label for="apellidos">Dirección Trabajo</label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input type="text" maxlength="50" value="{{$famili[0]['fam_titpadr']}}" id="titulopa" name="titulopa" class="form-control" style="width:80%;height:25px; margin-left: 30px"/>
                                                        </td>
                                                        <td>
                                                            <input type="text" value="{{$famili[0]['fam_dirofip']}}" id="dirofipa" name="dirofipa" class="form-control" style="width:80%;height:25px"/>
                                                        </td>
                                                    </tr>




                                                    <tr>
                                                        <td align="center" colspan="2">
                                                            <br>
                                                            <h4>Información del Madre</h4>
                                                            <br>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="" colspan="2" style="margin-left:0px;">
                                                            <h4>Datos Generales</h4>
                                                            <img src="{{asset('imgCopece/line.png')}}"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label style="margin-left:30px;" for="nombres">Nombres</label>
                                                        </td>
                                                        <td>
                                                            <label for="apellidos">Apellidos</label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input type="text"  maxlength="60" value="{{$famili[0]['fam_nommadr']}}" id="nombresma" name="nombresma" value="{{$famili[0]['']}}" class="form-control" style="width:80%;height:25px; margin-left: 30px" />
                                                        </td>
                                                        <td>
                                                            <input type="text" maxlength="60" class="form-control"  value="{{$famili[0]['fam_apematr']}}" id="apellidosma" name="apellidosma" style="width:80%;height:25px" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label style="margin-left:30px;" for="nombres">Número de Cédula</label>
                                                        </td>
                                                        <td>
                                                            <label for="apellidos">Dirección domiciliaria</label>
                                                        </td>
                                                    </tr>
                                                    <br>
                                                    <tr>
                                                        <td>
                                                            <input type="text" maxlength="15" value="{{$famili[0]['fam_ceduma']}}" id="cedulama" name="cedulama" class="form-control" style="width:80%;height:25px; margin-left: 30px"/>
                                                        </td>
                                                        <td>
                                                            <input type="text" maxlength="255" value="{{$famili[0]['fam_dirmadr']}}" id="direccionma" name="direccionma" class="form-control" style="width:80%;height:25px"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label style="margin-left:30px;" for="nombres">Teléfonos</label>
                                                        </td>
                                                        <td>
                                                            <label for="apellidos">Correo Electrónico</label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input type="text" maxlength="50" value="{{$famili[0]['fam_telmadr']}}" id="telefonoma" name="telefonoma" class="form-control" style="width:80%;height:25px; margin-left: 30px"/>
                                                        </td>
                                                        <td>
                                                            <input type="email" value="{{$famili[0]['fam_emailma']}}" id="emailma" name="emailma" class="form-control" style="width:80%;height:25px"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="" colspan="2" style="margin-left:0px;">
                                                            <br>
                                                            <h4>Datos Profesionales</h4>
                                                            <img src="{{asset('imgCopece/line.png')}}"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label style="margin-left:30px;" for="nombres">Lugar de Trabajo</label>
                                                        </td>
                                                        <td>
                                                            <label for="apellidos">Cargo</label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input type="text" maxlength="50" value="{{$famili[0]['fam_tramadr']}}" id="trabajoma" name="trabajoma" class="form-control" style="width:80%;height:25px; margin-left: 30px"/>
                                                        </td>
                                                        <td>
                                                            <input type="text" value="{{$famili[0]['fam_carmadr']}}" id="cargoma" name="cargoma" class="form-control" style="width:80%;height:25px"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label style="margin-left:30px;" for="nombres">Titulo</label>
                                                        </td>
                                                        <td>
                                                            <label for="apellidos">Dirección Trabajo</label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input type="text" maxlength="50" value="{{$famili[0]['fam_titmadr']}}" id="tituloma" name="tituloma" class="form-control" style="width:80%;height:25px; margin-left: 30px"/>
                                                        </td>
                                                        <td>
                                                            <input type="text" value="{{$famili[0]['fam_dirofim']}}" id="dirofima" name="dirofima" class="form-control" style="width:80%;height:25px"/>
                                                        </td>
                                                    </tr>



                                                    <input type="text" value="{{$famili[0]['fam_secuencia']}}" id="fam_secuencia" name="fam_secuencia" style="display:none;"/>
                                                    <tr>

                                                        <td align="center" style="border: hidden" colspan="2">
                                                            <br>
                                                            <button type="submit" id="guardar" class="btn btn-success" style="background-color: #010A35; width: 70%">Guardar</button>
                                                            <br>
                                                            <br>

                                                        </td>
                                                    </tr>

                                                </table>
                                            </form>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
@endsection
@section('js')
    @parent
    <script language="javascript" type="text/javascript">

        function validateForm(){
            if($("#nombrespa").val() == ''){
                alert("Campo nombres no puede ser vacio!");
                $("#nombrespa").focus();
                return false;
            }

            if($("#nombresma").val() == ''){
                alert("Campo nombres no puede ser vacio!");
                $("#nombresma").focus();
                return false;
            }
            if($("#apellidospa").val() == ''){
                alert("Campo apellidos no puede ser vacio!");
                $("#apellidospa").focus();
                return false;
            }
            if($("#apellidosma").val() == ''){
                alert("Campo apellidos no puede ser vacio!");
                $("#apellidosma").focus();
                return false;
            }
            if($("#cedulapa").val() == ''){
                alert("Campo cédula no puede ser vacio!");
                $("#cedulapa").focus();
                return false;
            }
            if($("#cedulama").val() == ''){
                alert("Campo cédula no puede ser vacio!");
                $("#cedulama").focus();
                return false;
            }

            if($("#direccionpa").val() == ''){
                alert("Campo dirección no puede ser vacio!");
                $("#direccionpa").focus();
                return false;
            }
            if($("#direccionma").val() == ''){
                alert("Campo dirección no puede ser vacio!");
                $("#direccionma").focus();
                return false;
            }

            if($("#telefonopa").val() == ''){
                alert("Campo teléfono no puede ser vacio!");
                $("#telefonopa").focus();
                return false;
            }
            if($("#telefonoma").val() == ''){
                alert("Campo teléfono no puede ser vacio!");
                $("#telefonoma").focus();
                return false;
            }

            if($("#emailpa").val() == ''){
                alert("Campo correo electrónico no puede ser vacio!");
                $("#emailpa").focus();
                return false;
            }
            if($("#emailma").val() == ''){
                alert("Campo correo electrónico no puede ser vacio!");
                $("#emailma").focus();
                return false;
            }

        }

    </script>
@endsection




