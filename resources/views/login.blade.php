@extends('templates.master')

@section('title', 'Login')
@section('page_title')
    <h1><i class="fas fa-lock-open"></i> Login</h1>
@endsection

@section('content')
    <table id="principal" width="100%" height="1000px" style="background-color: white">
        <tr>
            <td width="100%" height="100%">
                <table width="100%" height="100%">
                    <tr>

                        <td width="85%">
                            <table width="100%" height="100%">
                                <!--<tr>
                                    <td width="100%" height="80px" style="background-color: #010A35; vertical-align: center; align-content: left">
                                        <img src="{{asset("imgCopece/login_tit.png")}}" style=""/>
                                    </td>
                                </tr>-->
                                <tr>
                                    <td width="100%" align="center" height="580px" valign="top" style="background-color: #EDEDED">
                                        <br>
                                        <br>
                                        <br>
                                        <div id="message"></div>
                                        <br>
                                        <br>
                                        <form id="forIngresar" method="post" action="{{ action('acmfamiliController@valUsuario') }}" accept-charset="UTF-8">
                                        <!--<input type="hidden" name="_token" value="{{ csrf_token() }}">-->
                                            {{csrf_field()}}
                                            <div class="" style="width:500px; height:400px">
                                                <table style="background-color: white; border-collapse: collapse;  border-radius: 8px;  overflow: hidden; width: 100%; height:100%">
                                                    <tr height="100px">
                                                        <td align="center">
                                                            <h4>
                                                                BIENVENIDO
                                                            </h4>
                                                        </td>
                                                    </tr>
                                                    <tr height="20px">
                                                        <td align="left">
                                                            <label for="usuario"><h2>Usuario</h2></label>
                                                        </td>
                                                    </tr>
                                                    <tr height="50px">
                                                        <td align="center">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" style="width:70%" id="usuario" name="usuario" value="ADUMMORENO"  placeholder="Ingrese su Usuario">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr height="20px">
                                                        <td align="left">
                                                            <label for="usuario"><h2>Contraseña</h2></label>
                                                        </td>
                                                    </tr>
                                                    <tr height="50px">
                                                        <td align="center">
                                                            <div class="form-group">
                                                                <input type="password" style="width:70%" class="form-control" id="clave" name="clave" value="ADUMMORENO" placeholder="Ingrese su Contraseña">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr height="20px">
                                                        <td align="right">
                                                            <a href="{{action('acmfamiliController@enviar_clave')}}"><h3>¿ Olvidó su contraseña ?</h3></a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <button style="background-color: #010A35; width: 70%" id="btingresar" type="button" class="btn btn-primary">Ingresar</button>
                                                        </td>
                                                    </tr>

                                                </table>
                                            </div>
                                        </form>
                                       </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
@endsection
@section('js')
    @parent
    <script language="javascript" type="text/javascript">
        $("#btingresar").click(function(){
            var url = $("#forIngresar").attr('action');
            var data = $("#forIngresar").serialize();
            $.ajax({
                type: 'POST',
                url: url,
                data: data,
                dataType: 'json',
                success: function (data) {
                    if(data == "Usuario o Contraseña no válidas") {
                        $("#message").html('<h4>'+data+'</h4>');
                    }else {
                        window.location = "{{URL::to('/login')}}";
                    }
                },
                error: function (data) {
                    console.log(data);
                    alert('ERROR - '+data+' Comuniquese con el administrador');
                }
            });
        });
    </script>
@endsection

