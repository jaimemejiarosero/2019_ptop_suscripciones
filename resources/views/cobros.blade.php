@extends('templates.master')

@section('title', 'Pagos')
@section('page_title')
    <h1><i class="fas fa-tv"></i> Cobros Suscripciones</h1>
@endsection
@section('page_usuario')
    <h1><i class="far fa-user-circle"></i> {{$famili[0]['fam_user']}}</h1>
@endsection


@section('content')
       <table id="principal" width="100%" style="background-color: white;">
           <tr>
               <td width="100%" >
                   <table width="100%">
                       <tr>
                           @section('menu')
                                       <tr>
                                           <td style="vertical-align: center">
                                               <a href="{{action('acmfamiliController@login')}}" style="text-decoration: none">
                                                   <div style="width:100%; height:100%">
                                                       <table><tr><td>
                                                                   <img src="{{asset("imgCopece/ico_ini.png")}}" style="margin-left:10px"/>
                                                               </td><td><h4>Inicio</h4></td></tr></table>
                                                       <br>
                                                   </div>
                                               </a>
                                           </td>
                                       </tr>
                                           <tr>
                                               <td style="vertical-align: center">
                                                   <a href="{{action('suscripcionController@index')}}" style="text-decoration: none">
                                                       <div style="width:100%; height:100%">
                                                           <table><tr><td>
                                                                       <img src="{{asset("imgCopece/ico_per.png")}}" style="margin-left:10px"/>
                                                                   </td>
                                                                   <td><h4>Suscripciones</h4></td></tr></table>
                                                           <br>
                                                       </div>
                                                   </a>
                                               </td>
                                           </tr>
                                       <tr>
                                           <td style="vertical-align: center">
                                               <a href="{{action('acmfamiliController@logout')}}" style="text-decoration: none">
                                                   <div style="width:100%; height:100%">
                                                       <table><tr><td>
                                                                   <img src="{{asset("imgCopece/ico_sal.png")}}" style="margin-left:10px"/>
                                                               </td><td><h4>Salir</h4></td></tr></table>
                                                       <br>
                                                   </div>
                                               </a>
                                           </td>
                                       </tr>
                           @endsection
                           <td width="85%" style="vertical-align: top;background-color: #EDEDED">
                               <table width="100%" style="height:95%;">

                                   <tr>
                                       <td colspan="3" width="100%" style="background-color: #EDEDED">
                                           <br>
                                           <br>
                                           <div class="scrollable" style="overflow-y:auto;">
                                               <table style="background-color: #EDEDED; border-collapse: collapse;  border-radius: 8px;  overflow: hidden; width: 100%; height:100%">
                                               <!--DELTA-->
                                               @if(count($alumnasDelta) > 0)
                                                   <tr style="height: 20px">
                                                       <td align="left" width="20px">
                                                           <!--<img src="{{asset("imgCopece/b_ver.png")}}" style="margin-left:30px;width:20px"/>-->
                                                           <img src="{{asset("imgCopece/logo_delta.png")}}" style="margin-left:50px;width:40px"/>
                                                       </td>
                                                       <td align="left">
                                                           @if($urldelta != '')
                                                               <h4>Solicitudes de Pago Pendientes Delta </h4>
                                                           @else
                                                               <h4>Información de los Estudiantes Delta</h4>
                                                           @endif
                                                       </td>
                                                   </tr>
                                                   <tr style="vertical-align: top">
                                                       <td colspan="2">
                                                           <br>
                                                           <br>
                                                          <!-- ////////////////////////////////ESTUDIANTES///////////////////////////////////////////////////-->
                                                           @if($urldelta != '')
                                                               <div class="table-responsive" align="left" width="100%" >
                                                                   {{csrf_field()}}

                                                                   <table class="table table-bordered table-sm table-striped" style="font-size: small; width:90%;margin-left: 50px" >
                                                                       <thead class="table-striped" style="background-color: #9E997B;color: white">
                                                                       <tr>
                                                                           <th scope="col">Id</th>
                                                                           <th scope="col">Fecha</th>
                                                                           <th scope="col">Referencia</th>
                                                                           <th scope="col">Estado</th>
                                                                           <th></th>
                                                                       </tr>
                                                                       </thead>
                                                                       <tbody>
                                                                       <tr>
                                                                           <th scope="row"><h5>{{$aluPagosDelta[0]['requestId']}}</h5></th>
                                                                           <td><h5>{{$aluPagosDelta[0]['date']}}</h5></td>
                                                                           <td><h5>{{$aluPagosDelta[0]['reference']}}</h5></td>
                                                                           <td><h5>{{$aluPagosDelta[0]['status']}}</h5></td>
                                                                           <td align="center">
                                                                               <h4 style="font-size: 15px">El pago esta siendo procesado,<br>
                                                                                   por favor espere unos minitos.</h4>
                                                                               <!--<button type="button" id="btprocesardelta" class="btn btn-success">Continuar Pago</button>-->
                                                                               <!-- <button type="button" id="btcancelar" class="btn btn-danger">Cancelar Pago</button>-->
                                                                           </td>
                                                                       </tr>
                                                                       </tbody>
                                                                   </table>
                                                               </div>

                                                           @else
                                                               <div class="table-responsive" align="left" width="100%" >
                                                                       <table class="table table-bordered table-sm table-striped" style="font-size: small; width:90%; margin-left: 50px" >
                                                                           <thead class="table-striped" style="background-color: #9E997B;color: white">
                                                                           <tr>
                                                                               <th scope="col">Código</th>
                                                                               <th scope="col">Cédula</th>
                                                                               <th scope="col">Nombre</th>
                                                                               <th scope="col">Dirección</th>
                                                                               <th scope="col">Teléfono</th>
                                                                               <th scope="col">Curso</th>
                                                                               <th scope="col">Estado</th>
                                                                               <th scope="col">Valor</th>
                                                                               <th scope="col">Opciones</th>
                                                                           </tr>
                                                                           </thead>
                                                                           <tbody>

                                                                           @foreach ($alumnasDelta as $alumnadelta)
                                                                               <form id="for{{$suscripcion['requestId']}}" method="post" action="{{ action('suscripcionController@susCobros') }}" accept-charset="UTF-8" onsubmit="return validateForm('for{{$suscripcion['requestId']}}')">
                                                                                   {{csrf_field()}}
                                                                               <tr style="background-color: white">
                                                                                   <th scope="row">{{$alumnadelta['alu_codalum']}}
                                                                                       <input type="text" name="alu_codigo" value="{{$alumnadelta['alu_codalum']}}" style="display:none">
                                                                                       <input type="text" name="status" value="{{$suscripcion['status']}}" style="display:none">
                                                                                       <input type="text" name="token" value="{{$suscripcion['token']}}" style="display:none">
                                                                                       <input type="text" name="document" value="{{$suscripcion['document']}}" style="display:none">
                                                                                       <input type="text" name="documentType" value="{{$suscripcion['documentType']}}" style="display:none">
                                                                                       <input type="text" name="name" value="{{$suscripcion['name']}}" style="display:none">
                                                                                       <input type="text" name="surname" value="{{$suscripcion['surname']}}" style="display:none">
                                                                                       <input type="text" name="email" value="{{$suscripcion['email']}}" style="display:none">
                                                                                       <input type="text" name="mobile" value="{{$suscripcion['mobile']}}" style="display:none">
                                                                                       <input type="text" name="fam_secuencia" value="{{$suscripcion['fam_secuencia']}}" style="display:none">
                                                                                   </th>
                                                                                   <td><h5>{{$alumnadelta['alu_numcedu']}}</h5></td>
                                                                                   <td><h5>{{$alumnadelta['alu_nomcomp']}}</h5></td>
                                                                                   <td><h5>{{$alumnadelta['alu_direcci']}}</h5></td>
                                                                                   <td><h5>{{$alumnadelta['alu_numtelf']}}</h5></td>
                                                                                   <td><h5>{{$alumnadelta['minidescrip']}}</h5></td>
                                                                                   <td>@if($alumnadelta['alu_estadomatriculado']=='S') <h6 style="color: #2ca02c; font-size: small">COBRADO</h6> @else <h6 style="color: red; font-size: small">PENDIENTE</h6> @endif</td>
                                                                                   <th scope="row">$395.50</th>
                                                                                   <td scope="row">
                                                                                       @if($alumnadelta['alu_estadomatriculado']=='S') <p class="text-primary"></p>  @else <button type="submit"  class="btn btn-success" style="background-color: #2ca02c; width: 100%">Realizar Cobro</button></td> @endif

                                                                               </tr>
                                                                               </form>
                                                                           @endforeach

                                                                           </tbody>
                                                                       </table>
                                                               </div>
                                                           @endif
                                                       </td>
                                                   </tr>
                                                @endif
                                                    <!-- FIN DELTA-->

                               </table>
                                           </div>
                                       </td>
                                   </tr>
                               </table>
                           </td>
                       </tr>
                   </table>
               </td>
           </tr>
       </table>

@endsection
@section('js')
    @parent

    <script>
        $(document).ready(function () {


        });
    </script>
@endsection
