@extends('templates.master')

@section('title', 'Suscripciones')
@section('page_title')
    <h1><i class="far fa-calendar-alt"></i> Suscripciones</h1>
@endsection

@section('content')
    <table id="principal" width="100%" style="background-color: white" valign="top">
        <tr>
            <td width="100%" valign="top">
                <table width="100%">
                    <tr>
                    @section('menu')

                        <tr>
                            <td style="vertical-align: center">
                                <a href="{{action('acmfamiliController@login')}}" style="text-decoration: none">
                                    <div style="width:100%; height:100%">
                                        <table><tr><td>
                                                    <img src="{{asset("imgCopece/ico_ini.png")}}" style="margin-left:10px"/>
                                                </td><td><h4>Inicio</h4></td></tr></table>
                                        <br>
                                    </div>
                                </a>
                            </td>
                        </tr>

                        <tr>
                            <td style="vertical-align: center">
                                <a href="{{action('suscripcionController@index')}}" style="text-decoration: none">
                                    <div style="width:100%; height:100%">
                                        <table><tr><td>
                                                    <img src="{{asset("imgCopece/ico_per.png")}}" style="margin-left:10px"/>
                                                </td>
                                                <td><h4>Suscripciones</h4></td></tr></table>
                                        <br>
                                    </div>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: center">
                                <a href="{{action('acmfamiliController@logout')}}" style="text-decoration: none">
                                    <div style="width:100%; height:100%">
                                        <table><tr><td>
                                                    <img src="{{asset("imgCopece/ico_sal.png")}}" style="margin-left:10px"/>
                                                </td><td><h4>Salir</h4></td></tr></table>
                                        <br>
                                    </div>
                                </a>
                            </td>
                        </tr>
                    @endsection
                        <td width="85%" style="vertical-align: top">
                            <table width="100%">
                                <tr>
                                    <td colspan="3" align="center" width="100%" valign="top" style="background-color: #EDEDED">
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="table-responsive" align="left" style="width:100%" >
                                            <table class="table-bordered table-sm table-striped" style="vertical-align: center; font-size: small; width:90%; margin-left: 50px" >
                                                <thead class="table-striped" style="background-color: #9E997B;color: white">
                                                <tr>
                                                    <th scope="col">Código</th>
                                                    <th scope="col">Cliente</th>
                                                    <th scope="col">Estado</th>
                                                    <th scope="col">Token</th>
                                                    <th scope="col">Pagador</th>
                                                    <th scope="col">Correo</th>
                                                    <th scope="col">Teléfono</th>
                                                    <th scope="col">Opciones</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                @foreach ($suscripciones as $suscripcion)
                                                        <tr style="background-color: white">
                                                            <td><h5>{{$suscripcion['fam_secuencia']}}</h5></td>
                                                            <td><h5>{{$suscripcion['fam_nombre']}}</h5></td>
                                                            <td><h5>{{$suscripcion['status']}}</h5></td>
                                                            <td><h5>{{$suscripcion['token']}}</h5></td>
                                                            <td><h5>{{$suscripcion['surname']}} {{$suscripcion['name']}}</h5></td>
                                                            <td><h5>{{$suscripcion['email']}}</h5></td>
                                                            <td><h5>{{$suscripcion['mobile']}}</h5></td>
                                                            <td scope="row">
                                                                <a href="{{action('suscripcionController@verCobros',[$suscripcion['fam_secuencia']])}}" style="text-decoration: none"><button type="button"  class="btn btn-success" style="background-color: #2ca02c; width: 100%">Ver Facturas</button></a></td>
                                                        </tr>
                                                @endforeach

                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
@endsection
@section('js')
    @parent
    <script language="javascript" type="text/javascript">
        function validateForm(form){

            var url = $("#"+form).attr('action');
            var data = $("#"+form).serialize();
            $.ajax({
                headers: { 'X-CSRF-Token': $('input[name="_token"]').val() },
                type: 'POST',
                url: url,
                data: data,
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    if (data.status.status == 'APPROVED'){
                        message = 'El Cobro fue realizado con éxito ';
                    }else{
                        if (data.status.status == 'REJECTED'){
                            message = 'El Cobro fue rechazado por la entidad financiera';
                        }else{
                            message = 'El Cobro se encuentra siendo procesado por la entidad financiera';
                        }
                    }
                    window.location = "{{URL::to('/pagos/estado/')}}" + "/" + data.status.message;

                },
                error: function (data) {
                    alert("error");
                    console.log(data);
                    //window.location = "{{URL::to('/pagos/estado/')}}" + "/Su sessión a caducado, por favor ingrese nuevamente.";
                }
            });
            return false;
        }
    </script>
@endsection



