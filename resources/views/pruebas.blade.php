@extends('templates.master')

@section('title', 'Pruebas Redirección')
@section('page_title')
    <h1><i class="fas fa-lock-open"></i>    Pruebas Redirección</h1>
@endsection

@section('content')
    <table id="principal" width="100%" height="1000px" style="background-color: white">
        <tr>
            <td width="100%" height="100%">
                <table width="100%" height="100%">
                    <tr>
                        <td width="85%">
                            <table width="100%" height="100%">
                                <!--<tr>
                                    <td width="100%" height="80px" style="background-color: #010A35; vertical-align: center; align-content: left">
                                        <img src="{{asset("imgCopece/login_tit.png")}}" style=""/>
                                    </td>
                                </tr>-->
                                <tr>
                                    <td width="100%" align="center" height="580px" valign="top" style="background-color: #EDEDED">
                                        <form id="forDatos" method="post" action="{{ action('Controller@pagPruebas') }}" accept-charset="UTF-8">
                                            {{csrf_field()}}
                                            <div class="form-group">
                                                SESION
                                                <input type="text" class="form-control" style="width:30%; background: red" id="login" name="login" value="6dd490faf9cb87a9862245da41170ff2"  placeholder="login *">
                                                <input type="text" class="form-control" style="width:30%; background: red" id="secretKey" name="secretKey" value="024h1IlD"  placeholder="secretKey *">

                                                    COMPRADOR
                                                    <input type="text" class="form-control" style="width:30%" id="CdocumentType" name="CdocumentType" value=""  placeholder="documentType">
                                                    <input type="text" class="form-control" style="width:30%" id="Cdocument" name="Cdocument" value=""  placeholder="document">
                                                    <input type="text" class="form-control" style="width:30%" id="Cname" name="Cname" value=""  placeholder="name">
                                                    <input type="text" class="form-control" style="width:30%" id="Csurname" name="Csurname" value=""  placeholder="surname">
                                                    <input type="text" class="form-control" style="width:30%" id="Ccompany" name="Ccompany" value=""  placeholder="company">
                                                    <input type="text" class="form-control" style="width:30%" id="Cemail" name="Cemail" value="jaime.mejia@ppm.com.ec"  placeholder="email">
                                                    <input type="text" class="form-control" style="width:30%" id="Cmobile" name="Cmobile" value=""  placeholder="mobile">
                                                        Dirección COMPRADOR
                                                        <input type="text" class="form-control" style="width:30%" id="Cstreet" name="Cstreet" value=""  placeholder="street">
                                                        <input type="text" class="form-control" style="width:30%" id="Ccity" name="Ccity" value=""  placeholder="city">
                                                        <input type="text" class="form-control" style="width:30%" id="Cstate" name="Cstate" value=""  placeholder="state">
                                                        <input type="text" class="form-control" style="width:30%" id="CpostalCode" name="CpostalCode" value=""  placeholder="postalCode">
                                                        <input type="text" class="form-control" style="width:30%" id="Ccountry" name="Ccountry" value=""  placeholder="country">
                                                        <input type="text" class="form-control" style="width:30%" id="Cphone" name="Cphone" value=""  placeholder="phone">
                                                    PAGADOR
                                                    <input type="text" class="form-control" style="width:30%" id="PdocumentType" name="PdocumentType" value=""  placeholder="documentType">
                                                    <input type="text" class="form-control" style="width:30%" id="Pdocument" name="Pdocument" value=""  placeholder="document">
                                                    <input type="text" class="form-control" style="width:30%" id="Pname" name="Pname" value=""  placeholder="name">
                                                    <input type="text" class="form-control" style="width:30%" id="Psurname" name="Psurname" value=""  placeholder="surname">
                                                    <input type="text" class="form-control" style="width:30%" id="Pcompany" name="Pcompany" value=""  placeholder="company">
                                                    <input type="text" class="form-control" style="width:30%" id="Pemail" name="Pemail" value="jaime.mejia@ppm.com.ec"  placeholder="email">
                                                    <input type="text" class="form-control" style="width:30%" id="Pmobile" name="Pmobile" value=""  placeholder="mobile">
                                                        Dirección PAGADOR
                                                        <input type="text" class="form-control" style="width:30%" id="Pstreet" name="Pstreet" value=""  placeholder="street">
                                                        <input type="text" class="form-control" style="width:30%" id="Pcity" name="Pcity" value=""  placeholder="city">
                                                        <input type="text" class="form-control" style="width:30%" id="Pstate" name="Pstate" value=""  placeholder="state">
                                                        <input type="text" class="form-control" style="width:30%" id="PpostalCode" name="PpostalCode" value=""  placeholder="postalCode">
                                                        <input type="text" class="form-control" style="width:30%" id="Pcountry" name="Pcountry" value=""  placeholder="country">
                                                        <input type="text" class="form-control" style="width:30%" id="Pphone" name="Pphone" value=""  placeholder="phone">
                                                    DATOS DEL PAGO
                                                        <input type="text" class="form-control" style="width:30%; background: red" id="reference" name="reference" value="00001"  placeholder="reference *">
                                                        <input type="text" class="form-control" style="width:30%; background: red" id="description" name="description" value="prueba"  placeholder="description *">
                                                        Amount
                                                        <input type="text" class="form-control" style="width:30%; background: red" id="currency" name="currency" value="USD"  placeholder="currency *">
                                                        <input type="text" class="form-control" style="width:30%; background: red" id="total" name="total" value="100"  placeholder="total *">
                                                        Amount Taxes
                                                        <input type="text" class="form-control" style="width:30%" id="tkind1" name="tkind1" value=""  placeholder="tkind1">
                                                        <input type="text" class="form-control" style="width:30%" id="tamount1" name="tamount1" value=""  placeholder="tamount1">
                                                        <input type="text" class="form-control" style="width:30%" id="tbase1" name="tbase1" value=""  placeholder="tbase1">
                                                        <input type="text" class="form-control" style="width:30%" id="tkind2" name="tkind2" value=""  placeholder="tkind2">
                                                        <input type="text" class="form-control" style="width:30%" id="tamount2" name="tamount2" value=""  placeholder="tamount2">
                                                        <input type="text" class="form-control" style="width:30%" id="tbase2" name="tbase2" value=""  placeholder="tbase2">
                                                        Amount Details
                                                        <input type="text" class="form-control" style="width:30%" id="dkind1" name="dkind1" value="envio"  placeholder="dkind1">
                                                        <input type="text" class="form-control" style="width:30%" id="damount1" name="damount1" value=""  placeholder="damount1">
                                                        <input type="text" class="form-control" style="width:30%" id="dkind2" name="dkind2" value="propina"  placeholder="dkind2">
                                                        <input type="text" class="form-control" style="width:30%" id="damount2" name="damount2" value=""  placeholder="damount2">
                                                        <input type="text" class="form-control" style="width:30%" id="dkind3" name="dkind3" value="subtotal"  placeholder="dkind3">
                                                        <input type="text" class="form-control" style="width:30%" id="damount3" name="damount3" value=""  placeholder="damount3">
                                                    Items
                                                    <input type="text" class="form-control" style="width:30%" id="isku" name="isku" value=""  placeholder="sku">
                                                    <input type="text" class="form-control" style="width:30%" id="iname" name="iname" value=""  placeholder="iname">
                                                    <input type="text" class="form-control" style="width:30%" id="icategory" name="icategory" value=""  placeholder="icategory">
                                                    <input type="text" class="form-control" style="width:30%" id="iqty" name="iqty" value=""  placeholder="iqty">
                                                    <input type="text" class="form-control" style="width:30%" id="iprice" name="iprice" value=""  placeholder="iprice">
                                                    <input type="text" class="form-control" style="width:30%" id="itax" name="itax" value=""  placeholder="itax">
                                                RedirectRequest
                                                <input type="text" class="form-control" style="width:30%" id="locale" name="locale" value="es_EC"  placeholder="locale *">
                                                <input type="text" class="form-control" style="width:30%" id="allowPartial" name="allowPartial" value=""  placeholder="allowPartial">
                                                <input type="text" class="form-control" style="width:30%; background: red" id="ipAddress" name="ipAddress" value="127.0.0.1"  placeholder="ipAddress *">
                                                <input type="text" class="form-control" style="width:30%; background: red" id="userAgent" name="userAgent" value=""  placeholder="userAgent *">
                                                <input type="text" class="form-control" style="width:30%" id="paymentMethod" name="paymentMethod" value=""  placeholder="paymentMethod">
                                                <input type="text" class="form-control" style="width:30%; background: red" id="expiration" name="expiration" value="2018-05-07T15:56:41-05:00"  placeholder="expiration *">
                                                <input type="text" class="form-control" style="width:30%; background: red" id="returnUrl" name="returnUrl" value=""  placeholder="returnUrl *">
                                                <input type="text" class="form-control" style="width:30%" id="cancelUrl" name="cancelUrl" value=""  placeholder="cancelUrl">
                                                <input type="text" class="form-control" style="width:30%" id="skipResult" name="skipResult" value=""  placeholder="skipResult">
                                                <input type="text" class="form-control" style="width:30%" id="noBuyerFill" name="noBuyerFill" value=""  placeholder="noBuyerFill">

                                            </div>
                                            <div class="form-group">
                                                <button style="background-color: #010A35; width: 30%" id="btingresar" type="button" class="btn btn-primary">Pagar</button>
                                            </div>
                                        </form>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100%" align="center" height="580px" valign="top" style="background-color: #EDEDED">
                                        <form id="forConsultar" method="post" action="{{ action('Controller@conPruebas') }}" accept-charset="UTF-8">
                                            {{csrf_field()}}
                                            <div class="form-group">
                                                <input type="text" class="form-control" style="width:30%" id="requestId" name="requestId" value=""  placeholder="requestId">
                                            </div>
                                            <div class="form-group">
                                                <button style="background-color: #010A35; width: 30%" id="btconsultar" type="button" class="btn btn-primary">Consultar</button>
                                            </div>
                                        </form>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
@endsection
@section('js')
    @parent
    <script language="javascript" type="text/javascript">
        $("#btingresar").click(function(){
            var url = $("#forDatos").attr('action');
            var data = $("#forDatos").serialize();
            $.ajax({
                type: 'POST',
                url: url,
                data: data,
                dataType: 'json',
                success: function (data) {
                   console.log(data);
                    P.init(data.processUrl);
                },
                error: function (data) {
                    alert('ERROR - '+data+' Comuniquese con el administrador');
                }
            });
        });
        $("#btconsultar").click(function(){
            var url = $("#forConsultar").attr('action');
            var data = $("#forConsultar").serialize();
            $.ajax({
                type: 'POST',
                url: url,
                data: data,
                dataType: 'json',
                success: function (data) {
                   console.log(data);
                },
                error: function (data) {
                    //alert('ERROR - '+data+' Comuniquese con el administrador');
                }
            });
        });

        P.on('response', function(data) {

            var datajson = JSON.stringify(data);
            if(data['status']['status'] == 'REJECTED'){
               alert("REJECTED");
               console.log(datajson);
            }else{
                if(data['status']['status'] == 'APPROVED') {
                    alert("APROVED");
                    console.log(datajson);
                }else{
                    alert("PENDIENTE");
                    console.log(datajson);
                    //window.location = "{{URL::to('/pagos/estado/')}}" + "/" + data['status']['message'];
                }
            }
        });

    </script>
@endsection

