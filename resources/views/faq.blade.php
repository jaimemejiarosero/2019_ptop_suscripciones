@extends('templates.master')

@section('title', 'Historial')
@section('page_title')
    <h1><i class="far fa-question-circle"></i> Preguntas Frecuentes</h1>
@endsection
@section('page_usuario')
    <<h1><i class="far fa-user-circle"></i> {{$famili[0]['fam_user']}}</h1>
@endsection

@section('content')
    <table id="principal" width="100%" style="background-color: white" valign="top">
        <tr>
            <td width="100%" valign="top">
                <table width="100%">
                    <tr>
                    @section('menu')

                        <tr>
                            <td style="vertical-align: center">
                                <a href="{{action('acmfamiliController@login')}}" style="text-decoration: none">
                                    <div style="width:100%; height:100%">
                                        <table><tr><td>
                                                    <img src="{{asset("imgCopece/ico_ini.png")}}" style="margin-left:10px"/>
                                                </td><td><h4>Pagos</h4></td></tr></table>
                                        <br>
                                    </div>
                                </a>
                            </td>
                        </tr>

                        <tr>
                            <td style="vertical-align: center">
                                <a href="{{action('acmfamiliController@historial')}}" style="text-decoration: none">
                                    <div style="width:100%; height:100%">
                                        <table><tr><td>
                                                    <img src="{{asset("imgCopece/ico_his.png")}}" style="margin-left:10px"/>
                                                </td><td><h4>Historial Pagos</h4></td></tr></table>
                                        <br>
                                    </div>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: center">
                                <a href="{{action('acmfamiliController@faq')}}" style="text-decoration: none">
                                    <div style="width:100%; height:100%">
                                        <table><tr><td>
                                                    <img src="{{asset("imgCopece/ico_his.png")}}" style="margin-left:10px"/>
                                                </td>
                                                <td><h4>Preguntas Frecuentes</h4></td></tr></table>
                                        <br>
                                    </div>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: center">
                                <a href="{{action('acmfamiliController@logout')}}" style="text-decoration: none">
                                    <div style="width:100%; height:100%">
                                        <table><tr><td>
                                                    <img src="{{asset("imgCopece/ico_sal.png")}}" style="margin-left:10px"/>
                                                </td><td><h4>Salir</h4></td></tr></table>
                                        <br>
                                    </div>
                                </a>
                            </td>
                        </tr>
                    @endsection
                        <td width="85%" style="vertical-align: top">
                            <table width="100%">
                                <tr>
                                    <td colspan="3" align="center" width="100%" valign="top" style="background-color: #EDEDED">
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="table-responsive" align="left" style="width:100%" >
                                            <table class="table table-bordered table-sm table-striped" style="font-size: small; width:90%; margin-left: 50px" >
                                                <thead class="table-striped" style="background-color: white;color: #8D8D8D">
                                                <tr><td>
                                                        <p>
                                                        <h4 style="font-size: 17px">LISTADO DE PREGUNTAS FRECUENTES</h4>
                                                        </p>

                                                        <p>
                                                            1.	¿Qué es Place to Pay?
                                                            Place to Pay es la plataforma de pagos electrónicos que usa Green 9 para procesar en línea las transacciones generadas en la tienda virtual con las formas de pago habilitadas para tal fin.
                                                        </p>
                                                        <p>
                                                            2.	¿Cómo puedo pagar?
                                                            En la tienda virtual de Green 9 usted podrá realizar su pago utilizando: Tarjetas de Crédito de las siguientes franquicias: Diners, Visa y MasterCard; de todos los bancos en lo que corresponde a pago corriente y en cuanto a diferido, únicamente las tarjetas emitidas por Banco Pichincha, Loja, BGR y Machala. Pronto extenderemos esta lista de opciones para nuestros clientes.
                                                        </p>
                                                        <p>
                                                            3.	¿Es seguro ingresar mis datos bancarios en este sitio web?
                                                            Para proteger tus datos Green 9 delega en Place to Pay la captura de la información sensible. Nuestra plataforma de pagos cumple con los estándares exigidos por la norma internacional PCI DSS de seguridad en transacciones con tarjeta de crédito. Además tiene certificado de seguridad SSL expedido por GeoTrust una compañía Verisign, el cual garantiza comunicaciones seguras mediante la encriptación de todos los datos hacia y desde el sitio; de esta manera te podrás sentir seguro a la hora de ingresar la información de su tarjeta.

                                                            Durante el proceso de pago, en el navegador se muestra el nombre de la organización autenticada, la autoridad que lo certifica y la barra de dirección cambia a color verde. Estas características son visibles de inmediato, dan garantía y confianza para completar la transacción en Place to Pay.

                                                            Place to Pay también cuenta con el monitoreo constante de McAfee Secure y la firma de mensajes electrónicos con Certicámara.

                                                            Place to Pay es una marca de la empresa colombiana EGM Ingeniería Sin Fronteras S.A.S.
                                                        </p>
                                                        <p>
                                                            4.	¿Puedo realizar el pago cualquier día y a cualquier hora?
                                                            Sí, en Green 9 podrás realizar tus compras en línea los 7 días de la semana, las 24 horas del día a sólo un clic de distancia.
                                                        </p>
                                                        <p>
                                                            5.	¿Puedo cambiar la forma de pago?
                                                            Si aún no has finalizado tu pago, podrás volver al paso inicial y elegir la forma de pago que prefieras. Una vez finalizada la compra no es posible cambiar la forma de pago.
                                                        </p>
                                                        <p>
                                                            6.	¿Pagar electrónicamente tiene algún valor para mí como comprador?

                                                            No, los pagos electrónicos realizados a través de Place to Pay no generan costos adicionales para el comprador.
                                                        </p>
                                                        <p>
                                                            7.	¿Qué debo hacer si mi transacción no concluyó?
                                                            En primera instancia deberás revisar si llegó un mail de confirmación del pago en tu cuenta de correo electrónico (la inscrita en el momento de realizar el pago), en caso de no haberlo recibido, deberás contactar a soporte@placetopay.com para confirmar el estado de la transacción.
                                                        </p>
                                                        <p>
                                                            8.	¿Qué debo hacer si no recibí el comprobante de pago?
                                                            Por cada transacción aprobada a través de Place to Pay, recibirás un comprobante del pago con la referencia de compra en la dirección de correo electrónico que indicaste al momento de pagar. Si no lo recibes, podrás contactar a soporte@placetopay.com, para solicitar el reenvío del comprobante a la misma dirección de correo electrónico registrada al momento de pagar.

                                                        </p>
                                                    </td></tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
@endsection
@section('js')
    @parent
    <script language="javascript" type="text/javascript">
        $("#btaceptar").click(function(){
            window.location = "{{URL::to('/login/')}}";
        });
    </script>
@endsection



