<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Matriculación - @yield('title')</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">
    <link href="{{asset("css/style.css")}}" rel="stylesheet" type="text/css" />
</head>
<body >
        <table id="principal" width="100%" style="background-color: white">
            <tr>
                <td width="100%" height="100%">
                    <table width="100%" height="100%">
                        <tr>
                            <td width="15%" valign="top" align="left">
                                <table width="100%" style="height:65%" valign="top">
                                    <tr>
                                        <td valign="top" align="center">
                                            <img src="{{asset("imgCopece/logo.png")}}" style="width:100%"/>
                                            <img src="{{asset("imgCopece/logo_preescolar.png")}}" style="width:70%"/>
                                            <img src="{{asset("imgCopece/line.png")}}" style="width:100%"/>
                                            <br>
                                            <br>
                                            <br>
                                        </td>
                                    </tr>
                                    @yield('menu')
                                </table>
                            </td>
                            <td width="85%">
                                <table width="100%" height="100%">
                                    <tr>
                                        <td width="70%" height="80px" style="background-color: #010A35; vertical-align: center; align-content: left;">
                                            @yield('page_title')
                                        </td>
                                        <td width="30%" height="80px" style="background-color: #010A35; vertical-align: center; align-content: left;">
                                            @yield('page_usuario')
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" width="100%" align="center" height="580px" valign="top" style="background-color: #EDEDED">
                                            @yield('content')
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

    @section('js')
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script src="https://secure.placetopay.ec/redirection/lightbox.min.js"></script>


        <script>
            function deshabilitaRetroceso(){
                window.location.hash="no-back-button";
                window.location.hash="Again-No-back-button" //chrome
                window.onhashchange=function(){window.location.hash="no-back-button";}
            }
        </script>
    @show
</body>
</html>

