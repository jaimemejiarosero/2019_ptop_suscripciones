@extends('templates.master')

@section('title', 'Historial')
@section('page_title')
    <h1><i class="far fa-calendar-alt"></i> Historial de Pagos</h1>
@endsection
@section('page_usuario')
    <<h1><i class="far fa-user-circle"></i> {{$famili[0]['fam_user']}}</h1>
@endsection

@section('content')
    <table id="principal" width="100%" style="background-color: white" valign="top">
        <tr>
            <td width="100%" valign="top">
                <table width="100%">
                    <tr>
                    @section('menu')

                        <tr>
                            <td style="vertical-align: center">
                                <a href="{{action('acmfamiliController@login')}}" style="text-decoration: none">
                                    <div style="width:100%; height:100%">
                                        <table><tr><td>
                                                    <img src="{{asset("imgCopece/ico_ini.png")}}" style="margin-left:10px"/>
                                                </td><td><h4>Pagos</h4></td></tr></table>
                                        <br>
                                    </div>
                                </a>
                            </td>
                        </tr>

                        <tr>
                            <td style="vertical-align: center">
                                <a href="{{action('acmfamiliController@historial')}}" style="text-decoration: none">
                                    <div style="width:100%; height:100%">
                                        <table><tr><td>
                                                    <img src="{{asset("imgCopece/ico_his.png")}}" style="margin-left:10px"/>
                                                </td><td><h4>Historial Pagos</h4></td></tr></table>
                                        <br>
                                    </div>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: center">
                                <a href="{{action('acmfamiliController@faq')}}" style="text-decoration: none">
                                    <div style="width:100%; height:100%">
                                        <table><tr><td>
                                                    <img src="{{asset("imgCopece/ico_his.png")}}" style="margin-left:10px"/>
                                                </td>
                                                <td><h4>Preguntas Frecuentes</h4></td></tr></table>
                                        <br>
                                    </div>
                                </a>
                            </td>
                        </tr>

                        <tr>
                            <td style="vertical-align: center">
                                <a href="{{action('acmfamiliController@logout')}}" style="text-decoration: none">
                                    <div style="width:100%; height:100%">
                                        <table><tr><td>
                                                    <img src="{{asset("imgCopece/ico_sal.png")}}" style="margin-left:10px"/>
                                                </td><td><h4>Salir</h4></td></tr></table>
                                        <br>
                                    </div>
                                </a>
                            </td>
                        </tr>
                    @endsection
                        <td width="85%" style="vertical-align: top">
                            <table width="100%">
                                <!--<tr>
                                    <td colspan="3">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Tipos
                                            </button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="#" value="PN">Pagos Normales</a>
                                                <a class="dropdown-item" href="#">Pagos Recurrentes</a>
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item" href="#">Suscripciones</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>-->
                                <tr>
                                    <td colspan="3" align="center" width="100%" valign="top" style="background-color: #EDEDED">
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="table-responsive" align="left" style="width:100%" >
                                            <table class="table table-bordered table-sm table-striped" style="font-size: small; width:90%; margin-left: 50px" >
                                                <thead class="table-striped" style="background-color: #9E997B;color: white">
                                                <tr>
                                                    <th scope="col">Id</th>
                                                    <th scope="col">Estado</th>
                                                    <th scope="col">Referencia</th>
                                                    <th scope="col">Fecha</th>
                                                    <th scope="col">Valor</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                @foreach ($pagosRegistrados as $pagosRegistrado)
                                                    <tr style="background-color: white">
                                                        <td><h5>{{$pagosRegistrado['requestId']}}</h5></td>
                                                        <td><h5>{{$pagosRegistrado['message']}}</h5></td>
                                                        <td><h5>{{$pagosRegistrado['reference']}}</h5></td>
                                                        <td><h5>{{$pagosRegistrado['date']}}</h5></td>
                                                        <th scope="row">${{$pagosRegistrado['monto']}}</th>

                                                    </tr>
                                                @endforeach

                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
@endsection
@section('js')
    @parent
    <script language="javascript" type="text/javascript">
        $("#btaceptar").click(function(){
            window.location = "{{URL::to('/login/')}}";
        });
    </script>
@endsection



