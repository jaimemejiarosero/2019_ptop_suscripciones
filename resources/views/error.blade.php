@extends('templates.master')

@section('title', 'Error')

@section('page_title')
    <h1><i class="far fa-question-circle"></i> Error</h1>
@endsection
@section('page_usuario')
    <<h1><i class="far fa-user-circle"></i> {{$famili[0]['fam_user']}}</h1>
@endsection

@section('content')
    <table id="principal" width="100%" style="background-color: white" valign="top">
        <tr>
            <td width="100%" valign="top">
                <table width="100%">
                    <tr>
                    @section('menu')

                        <tr>
                            <td style="vertical-align: center">
                                <a href="{{action('acmfamiliController@login')}}" style="text-decoration: none">
                                    <div style="width:100%; height:100%">
                                        <table><tr><td>
                                                    <img src="{{asset("imgCopece/ico_ini.png")}}" style="margin-left:10px"/>
                                                </td><td><h4>Inicio</h4></td></tr></table>
                                        <br>
                                    </div>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: center">
                                <a href="{{action('acmfamiliController@perfil')}}" style="text-decoration: none">
                                    <div style="width:100%; height:100%">
                                        <table><tr><td>
                                                    <img src="{{asset("imgCopece/ico_per.png")}}" style="margin-left:10px"/>
                                                </td><td><h4>Perfil</h4></td></tr></table>
                                        <br>
                                    </div>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: center">
                                <a href="{{action('acmfamiliController@historial')}}" style="text-decoration: none">
                                    <div style="width:100%; height:100%">
                                        <table><tr><td>
                                                    <img src="{{asset("imgCopece/ico_his.png")}}" style="margin-left:10px"/>
                                                </td><td><h4>Historial Pagos</h4></td></tr></table>
                                        <br>
                                    </div>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: center">
                                <a href="{{action('acmfamiliController@faq')}}" style="text-decoration: none">
                                    <div style="width:100%; height:100%">
                                        <table><tr><td>
                                                    <img src="{{asset("imgCopece/ico_his.png")}}" style="margin-left:10px"/>
                                                </td>
                                                <td><h4>Preguntas Frecuentes</h4></td></tr></table>
                                        <br>
                                    </div>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: center">
                                <a href="{{action('acmfamiliController@logout')}}" style="text-decoration: none">
                                    <div style="width:100%; height:100%">
                                        <table><tr><td>
                                                    <img src="{{asset("imgCopece/ico_sal.png")}}" style="margin-left:10px"/>
                                                </td><td><h4>Salir</h4></td></tr></table>
                                        <br>
                                    </div>
                                </a>
                            </td>
                        </tr>
                    @endsection
                        <td width="85%" style="vertical-align: top">
                            <table width="100%">
                                <tr>
                                    <td colspan="3" align="center" width="100%" valign="top" style="background-color: #EDEDED">
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="" style="width:500px; height:400px">
                                            <table style="background-color: white; border-collapse: collapse;  border-radius: 8px;  overflow: hidden; width: 100%; height:100%">
                                                <tr height="100px">
                                                    <td align="center">
                                                        <h4>
                                                            ERROR SISTEMA
                                                        </h4>
                                                        <div id="contenido">{{$message}}</div>
                                                        <button type="button" id="btaceptar" class="btn btn-success">Aceptar</button>
                                                    </td>
                                                </tr>

                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
@endsection
@section('js')
    @parent
    <script language="javascript" type="text/javascript">
        $("#btaceptar").click(function(){
            window.location = "{{URL::to('/login/')}}";
        });
    </script>
@endsection

