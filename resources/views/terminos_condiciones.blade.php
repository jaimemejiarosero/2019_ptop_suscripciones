@extends('templates.master')

@section('title', 'Términos y Condiciones')
@section('page_title')
    <h1><i class="far fa-question-circle"></i> Términos y Condiciones</h1>
@endsection
@section('page_usuario')
    <<h1><i class="far fa-user-circle"></i> {{$famili[0]['fam_user']}}</h1>
@endsection

@section('content')
    <table id="principal" width="100%" style="background-color: white" valign="top">
        <tr>
            <td width="100%" valign="top">
                <table width="100%">
                    <tr>
                    @section('menu')

                        <tr>
                            <td style="vertical-align: center">
                                <a href="{{action('acmfamiliController@login')}}" style="text-decoration: none">
                                    <div style="width:100%; height:100%">
                                        <table><tr><td>
                                                    <img src="{{asset("imgCopece/ico_ini.png")}}" style="margin-left:10px"/>
                                                </td><td><h4>Inicio</h4></td></tr></table>
                                        <br>
                                    </div>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: center">
                                <a href="{{action('acmfamiliController@perfil')}}" style="text-decoration: none">
                                    <div style="width:100%; height:100%">
                                        <table><tr><td>
                                                    <img src="{{asset("imgCopece/ico_per.png")}}" style="margin-left:10px"/>
                                                </td><td><h4>Perfil</h4></td></tr></table>
                                        <br>
                                    </div>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: center">
                                <a href="{{action('acmfamiliController@historial')}}" style="text-decoration: none">
                                    <div style="width:100%; height:100%">
                                        <table><tr><td>
                                                    <img src="{{asset("imgCopece/ico_his.png")}}" style="margin-left:10px"/>
                                                </td><td><h4>Historial Pagos</h4></td></tr></table>
                                        <br>
                                    </div>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: center">
                                <a href="{{action('acmfamiliController@faq')}}" style="text-decoration: none">
                                    <div style="width:100%; height:100%">
                                        <table><tr><td>
                                                    <img src="{{asset("imgCopece/ico_his.png")}}" style="margin-left:10px"/>
                                                </td>
                                                <td><h4>Preguntas Frecuentes</h4></td></tr></table>
                                        <br>
                                    </div>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: center">
                                <a href="{{action('acmfamiliController@logout')}}" style="text-decoration: none">
                                    <div style="width:100%; height:100%">
                                        <table><tr><td>
                                                    <img src="{{asset("imgCopece/ico_sal.png")}}" style="margin-left:10px"/>
                                                </td><td><h4>Salir</h4></td></tr></table>
                                        <br>
                                    </div>
                                </a>
                            </td>
                        </tr>
                    @endsection
                        <td width="85%" style="vertical-align: top">
                            <table width="100%">
                                <tr>
                                    <td align="center" width="100%" valign="top" style="background-color: #EDEDED">
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="table-responsive" align="center" style="width:100%" >
                                            <table class="table table-bordered table-sm table-striped" style="font-size: small; width:60%; margin-left: 50px" >
                                                <thead class="" style="background-color: white;color: #8D8D8D">
                                                <tr><td width="80%" align="center">
                                                        <p>
                                                        <h4 style="font-size: 20px">TERMINOS Y CONDICIONES</h4>
                                                        </p>
                                                        <br>
                                                        <p>
                                                            <h5 style="font-size: 18px">Este servicio tiene por objeto el pago on-line de los recibos por servicios académicos emitidos por COPECE.
                                                            El uso de este servicio implica la aceptación de los términos y condiciones
                                                            del servicio. Si no está de acuerdo con los presentes Términos y Condiciones, no acceda a ninguna
                                                            de las páginas de la Pasarela de Pago en Internet.</h5>
                                                        </p>
                                                        <br>
                                                        <p>
                                                        <h5 style="font-size: 18px">Queda prohibido todo uso del Servicio con un propósito distinto del recogido en este apartado, o con
                                                            fines ilícitos, o que perjudiquen o impidan, puedan dañar y/o sobrecargar el funcionamiento del
                                                            mismo.</h5>
                                                        </p>
                                                        <br>
                                                        <br>
                                                        <p>
                                                        <h4>Políticas de devolución y cancelación:</h4>
                                                        <h5 style="font-size: 18px">La causa de devolución del importe abonado por los servicios contratados, debidos a equivocación o
                                                            no conformidad con lo solicitado, así como el procedimiento a seguir para las devoluciones o
                                                            reclamaciones a que se tuviera lugar, se regirán por lo establecido al efecto en las normas de
                                                            matrícula y orden de precios vigentes en el momento del servicio contratado.</h5>
                                                        </p>
                                                        <br>
                                                        <br>
                                                        <p>
                                                        <h4>Edad mínima del solicitante y moneda de transacción:</h4>
                                                        <h5 style="font-size: 18px">La edad mínima para solicitar los servicios que se prestan en esta pasarela de pago es de 18 años.
                                                            La moneda de transacción de los servicios que se contraten será el dolar (USD).</h5>
                                                        </p>
                                                        <button type="button" id="btcerrar" class="btn btn-success">Aceptar</button>
                                                        <br>
                                                        <br>
                                                    </td></tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
@endsection
@section('js')
    @parent
    <script language="javascript" type="text/javascript">
        $("#btcerrar").click(function(){
            window.location = "{{URL::to('/login/')}}";
        });
    </script>
@endsection



