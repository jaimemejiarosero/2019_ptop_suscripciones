@extends('templates.master')

@section('title', 'Recuperación Contraseña')
@section('page_title')
    <h1><i class="fas fa-lock-open"></i> Login</h1>
@endsection

@section('content')
    <table id="principal" width="100%" height="1000px" style="background-color: white">
        <tr>
            <td width="100%" height="100%">
                <table width="100%" height="100%">
                    <tr>

                        <td width="85%">
                            <table width="100%" height="100%">
                                <!--<tr>
                                    <td width="100%" height="80px" style="background-color: #010A35; vertical-align: center; align-content: left">
                                        <img src="{{asset("imgCopece/login_tit.png")}}" style=""/>
                                    </td>
                                </tr>-->
                                <tr>
                                    <td width="100%" align="center" height="580px" valign="top" style="background-color: #EDEDED">
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <form id="forRecuperar" method="post" action="{{ action('acmfamiliController@valEnvio') }}" accept-charset="UTF-8">
                                            {{csrf_field()}}
                                            <div class="" style="width:500px; height:400px">
                                                <table style="background-color: white; border-collapse: collapse;  border-radius: 8px;  overflow: hidden; width: 100%; height:100%">
                                                    <tr height="100px">
                                                        <td align="center">
                                                            <h4>
                                                                Recuperación de Contraseña
                                                            </h4>
                                                        </td>
                                                    </tr>
                                                    <tr height="20px">
                                                        <td align="left">
                                                            <label for="usuario"><h2>Cédula</h2></label>
                                                        </td>
                                                    </tr>
                                                    <tr height="50px">
                                                        <td align="center">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" style="width:70%" id="cedula" name="cedula" value=""  placeholder="Ingrese su Cédula">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr height="20px">
                                                        <td align="left">
                                                            <label for="usuario"><h2>Correo Electrónico</h2></label>
                                                        </td>
                                                    </tr>
                                                    <tr height="50px">
                                                        <td align="center">
                                                            <div class="form-group">
                                                                <input type="email" style="width:70%" class="form-control" id="email" name="email" value="" placeholder="Ingrese su Correo electrónico">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td align="center">
                                                            <button style="background-color: #010A35; width: 70%" type="button" id="btrecuperar" class="btn btn-primary">Recuperar</button>
                                                        </td>
                                                    </tr>

                                                </table>
                                            </div>
                                        </form>
                                       </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
@endsection
@section('js')
    @parent
    <script language="javascript" type="text/javascript">
        $("#btrecuperar").click(function(){
            var url = $("#forRecuperar").attr('action');
            var data = $("#forRecuperar").serialize();
            $.ajax({
                type: 'POST',
                url: url,
                data: data,
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    alert(data);
                },
                error: function (data) {
                    console.log('ERROR -'+data);
                }
            });
        });
    </script>
@endsection




